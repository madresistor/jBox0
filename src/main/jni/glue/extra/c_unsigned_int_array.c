/*
 * This file is part of jBox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_extra_CUnsignedIntArray.h"
#include "private.h"

/*
 * Class:     com_madresistor_box0_extra_CUnsignedIntArray
 * Method:    getValue
 * Signature: (Ljava/nio/ByteBuffer;J)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_extra_CUnsignedIntArray_getValue
  (JNIEnv *env, jclass cls, jobject obj, jlong index)
{
	UNUSED(cls);

	unsigned int *array = (*env)->GetDirectBufferAddress(env, obj);
	return array[index];
}

jobject c_unsigned_int_array(JNIEnv *env, unsigned int *values, size_t count)
{
	jobject arg = (*env)->NewDirectByteBuffer(env, values, sizeof(*values) * count);
	if (arg == JAVA_NULL) {
		return JAVA_NULL;
	}

	jclass cls =  (*env)->FindClass(env, "com/madresistor/box0/extra/CUnsignedIntArray");
	if (cls == JAVA_NULL) {
		return JAVA_NULL;
	}

	jmethodID init =  (*env)->GetMethodID(env, cls, "<init>", "(Ljava/nio/ByteBuffer;J)V");
	if ((*env)->ExceptionCheck(env)) {
		return JAVA_NULL;
	}

	jobject res = (*env)->NewObject(env, cls, init, arg, (jlong) count);

	(*env)->DeleteLocalRef(env, cls);
	(*env)->DeleteLocalRef(env, arg);

	return res;
}
