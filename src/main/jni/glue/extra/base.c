/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_extra_Base.h"
#include "private.h"

/*
 * Class:     com_madresistor_box0_extra_Base
 * Method:    equals
 * Signature: (Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Z
 */
JNIEXPORT jboolean JNICALL Java_com_madresistor_box0_extra_Base_equals
  (JNIEnv *env, jclass cls, jobject _ptr1, jobject _ptr2)
{
	UNUSED(cls);

	void *ptr1 = (*env)->GetDirectBufferAddress(env, _ptr1);
	void *ptr2 = (*env)->GetDirectBufferAddress(env, _ptr2);

	return BOOL_C_TO_JAVA(ptr1 == ptr2);
}
