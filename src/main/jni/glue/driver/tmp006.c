/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_driver_Tmp006.h"
#include "private.h"
#include <libbox0/libbox0.h>
#include <libbox0/driver/tmp006/tmp006.h>

/*
 * Class:     com_madresistor_box0_driver_Tmp006
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;DZI)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_driver_Tmp006_open
  (JNIEnv *env, jclass cls, jobject obj, jdouble calib_factor, jboolean ADR1, jint ADR0)
{
	UNUSED(cls);
	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	b0_tmp006 *tmp006;
	RESULT_EXCEPTION_ACT(b0_tmp006_open(i2c, &tmp006, calib_factor, ADR1, ADR0), 0);

	LOG_DEBUG("tmp006 is %p", tmp006);
	return (*env)->NewDirectByteBuffer(env, tmp006, 0);
}

/*
 * Class:     com_madresistor_box0_driver_Tmp006
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Tmp006_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_tmp006 *tmp006 = EXTRACT_TMP006(env, obj);
	LOG_DEBUG("tmp006 is %p", tmp006);

	RESULT_EXCEPTION_ACT(b0_tmp006_close(tmp006));
}

/*
 * Class:     com_madresistor_box0_driver_Tmp006
 * Method:    read
 * Signature: (Ljava/nio/ByteBuffer;)D
 */
JNIEXPORT jdouble JNICALL Java_com_madresistor_box0_driver_Tmp006_read
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_tmp006 *tmp006 = EXTRACT_TMP006(env, obj);
	LOG_DEBUG("tmp006 is %p", tmp006);

	double value;
	RESULT_EXCEPTION_ACT(b0_tmp006_read(tmp006, &value), 0);
	return value;
}
