/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_driver_Si114x.h"
#include "private.h"
#include <libbox0/libbox0.h>
#include <libbox0/driver/si114x/si114x.h>

/*
 * Class:     com_madresistor_box0_driver_Si114x
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_driver_Si114x_open
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	b0_si114x *si114x;
	RESULT_EXCEPTION_ACT(b0_si114x_open(i2c, &si114x), 0);

	LOG_DEBUG("si114x is %p", si114x);
	return (*env)->NewDirectByteBuffer(env, si114x, 0);
}

/*
 * Class:     com_madresistor_box0_driver_Si114x
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Si114x_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_si114x *si114x = EXTRACT_SI1145(env, obj);
	LOG_DEBUG("si114x is %p", si114x);

	RESULT_EXCEPTION_ACT(b0_si114x_close(si114x));
}

/*
 * Class:     com_madresistor_box0_driver_Si114x
 * Method:    readUvIndex
 * Signature: (Ljava/nio/ByteBuffer;)D
 */
JNIEXPORT jdouble JNICALL Java_com_madresistor_box0_driver_Si114x_readUvIndex
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_si114x *si114x = EXTRACT_SI1145(env, obj);
	LOG_DEBUG("si114x is %p", si114x);

	double value;
	RESULT_EXCEPTION_ACT(b0_si114x_uv_index_read(si114x, &value), 0);
	return value;
}

/*
 * Class:     com_madresistor_box0_driver_Si114x
 * Method:    readProximity
 * Signature: (Ljava/nio/ByteBuffer;)D
 */
JNIEXPORT jdouble JNICALL Java_com_madresistor_box0_driver_Si114x_readProximity
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_si114x *si114x = EXTRACT_SI1145(env, obj);
	LOG_DEBUG("si114x is %p", si114x);

	double value;
	RESULT_EXCEPTION_ACT(b0_si114x_proximity_read(si114x, &value), 0);
	return value;
}
