/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_driver_Adxl345.h"
#include "private.h"
#include <libbox0/libbox0.h>
#include <libbox0/driver/adxl345/adxl345.h>

/*
 * Class:     com_madresistor_box0_driver_Adxl345
 * Method:    openI2c
 * Signature: (Ljava/nio/ByteBuffer;Z)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_driver_Adxl345_openI2c
  (JNIEnv *env, jclass cls, jobject obj, jboolean ALT_ADDRESS)
{
	UNUSED(cls);
	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	b0_adxl345 *adxl345;
	RESULT_EXCEPTION_ACT(b0_adxl345_open_i2c(i2c, &adxl345, BOOL_JAVA_TO_C(ALT_ADDRESS)), 0);

	LOG_DEBUG("adxl345 is %p", adxl345);
	return (*env)->NewDirectByteBuffer(env, adxl345, 0);
}

/*
 * Class:     com_madresistor_box0_driver_Adxl345
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Adxl345_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_adxl345 *adxl345 = EXTRACT_ADXL345(env, obj);
	LOG_DEBUG("adxl345 is %p", adxl345);

	RESULT_EXCEPTION_ACT(b0_adxl345_close(adxl345));
}

/*
 * Class:     com_madresistor_box0_driver_Adxl345
 * Method:    powerUp
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Adxl345_powerUp
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_adxl345 *adxl345 = EXTRACT_ADXL345(env, obj);
	LOG_DEBUG("adxl345 is %p", adxl345);

	RESULT_EXCEPTION_ACT(b0_adxl345_power_up(adxl345));
}

/*
 * Class:     com_madresistor_box0_driver_Adxl345
 * Method:    read
 * Signature: (Ljava/nio/ByteBuffer;[D)V
 */
/* note: expecting _values array of minimum length 3 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Adxl345_read
  (JNIEnv *env, jclass cls, jobject obj, jdoubleArray _values)
{
	double values[3];

	UNUSED(cls);
	b0_adxl345 *adxl345 = EXTRACT_ADXL345(env, obj);
	LOG_DEBUG("adxl345 is %p", adxl345);

	RESULT_EXCEPTION_ACT(b0_adxl345_read(adxl345, &values[0], &values[1], &values[2]));

	/* copy back the values */
	(*env)->SetDoubleArrayRegion(env, _values, 0, 3, values);
}
