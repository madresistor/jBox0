/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_driver_L3gd20.h"
#include "private.h"
#include <libbox0/libbox0.h>
#include <libbox0/driver/l3gd20/l3gd20.h>

/*
 * Class:     com_madresistor_box0_driver_L3gd20
 * Method:    openI2c
 * Signature: (Ljava/nio/ByteBuffer;Z)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_driver_L3gd20_openI2c
  (JNIEnv *env, jclass cls, jobject obj, jboolean SAD0)
{
	UNUSED(cls);
	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	b0_l3gd20 *l3gd20;
	RESULT_EXCEPTION_ACT(b0_l3gd20_open_i2c(i2c, &l3gd20, BOOL_JAVA_TO_C(SAD0)), 0);

	LOG_DEBUG("l3gd20 is %p", l3gd20);
	return (*env)->NewDirectByteBuffer(env, l3gd20, 0);
}

/*
 * Class:     com_madresistor_box0_driver_L3gd20
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_L3gd20_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_l3gd20 *l3gd20 = EXTRACT_L3GD20(env, obj);
	LOG_DEBUG("l3gd20 is %p", l3gd20);

	RESULT_EXCEPTION_ACT(b0_l3gd20_close(l3gd20));
}

/*
 * Class:     com_madresistor_box0_driver_L3gd20
 * Method:    powerUp
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_L3gd20_powerUp
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_l3gd20 *l3gd20 = EXTRACT_L3GD20(env, obj);
	LOG_DEBUG("l3gd20 is %p", l3gd20);

	RESULT_EXCEPTION_ACT(b0_l3gd20_power_up(l3gd20));
}

/*
 * Class:     com_madresistor_box0_driver_L3gd20
 * Method:    read
 * Signature: (Ljava/nio/ByteBuffer;[D)V
 */
/* note: expecting _values array of minimum length 3 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_L3gd20_read
  (JNIEnv *env, jclass cls, jobject obj, jdoubleArray _values)
{
	double values[3];

	UNUSED(cls);
	b0_l3gd20 *l3gd20 = EXTRACT_L3GD20(env, obj);
	LOG_DEBUG("l3gd20 is %p", l3gd20);

	RESULT_EXCEPTION_ACT(b0_l3gd20_read(l3gd20, &values[0], &values[1], &values[2]));

	/* copy back the values */
	(*env)->SetDoubleArrayRegion(env, _values, 0, 3, values);
}
