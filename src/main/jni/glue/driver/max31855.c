/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_driver_Max31855.h"
#include "private.h"
#include <libbox0/libbox0.h>
#include <libbox0/driver/max31855/max31855.h>

/*
 * Class:     com_madresistor_box0_driver_Max31855
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_driver_Max31855_open
  (JNIEnv *env, jclass cls, jobject obj, jint addr)
{
	UNUSED(cls);
	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	b0_max31855 *max31855;
	RESULT_EXCEPTION_ACT(b0_max31855_open(spi, &max31855, addr), 0);

	LOG_DEBUG("max31855 is %p", max31855);
	return (*env)->NewDirectByteBuffer(env, max31855, 0);
}

/*
 * Class:     com_madresistor_box0_driver_Max31855
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Max31855_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_max31855 *max31855 = EXTRACT_MAX31855(env, obj);
	LOG_DEBUG("max31855 is %p", max31855);

	RESULT_EXCEPTION_ACT(b0_max31855_close(max31855));
}

/*
 * Class:     com_madresistor_box0_driver_Max31855
 * Method:    read
 * Signature: (Ljava/nio/ByteBuffer;)D
 */
JNIEXPORT jdouble JNICALL Java_com_madresistor_box0_driver_Max31855_read
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_max31855 *max31855 = EXTRACT_MAX31855(env, obj);
	LOG_DEBUG("max31855 is %p", max31855);

	double value;
	RESULT_EXCEPTION_ACT(b0_max31855_read(max31855, &value), 0);
	return value;
}
