/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_driver_Lm75.h"
#include "private.h"
#include <libbox0/libbox0.h>
#include <libbox0/driver/lm75/lm75.h>

/*
 * Class:     com_madresistor_box0_driver_Lm75
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;ZZZ)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_driver_Lm75_open
  (JNIEnv *env, jclass cls, jobject obj, jboolean SA2, jboolean SA1, jboolean SA0)
{
	UNUSED(cls);
	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	b0_lm75 *lm75;
	RESULT_EXCEPTION_ACT(b0_lm75_open(i2c, &lm75,
		BOOL_JAVA_TO_C(SA2), BOOL_JAVA_TO_C(SA1), BOOL_JAVA_TO_C(SA0)), 0);

	LOG_DEBUG("lm75 is %p", lm75);
	return (*env)->NewDirectByteBuffer(env, lm75, 0);
}

/*
 * Class:     com_madresistor_box0_driver_Lm75
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Lm75_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_lm75 *lm75 = EXTRACT_LM75(env, obj);
	LOG_DEBUG("lm75 is %p", lm75);

	RESULT_EXCEPTION_ACT(b0_lm75_close(lm75));
}

/*
 * Class:     com_madresistor_box0_driver_Lm75
 * Method:    read
 * Signature: (Ljava/nio/ByteBuffer;)D
 */
JNIEXPORT jdouble JNICALL Java_com_madresistor_box0_driver_Lm75_read
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_lm75 *lm75 = EXTRACT_LM75(env, obj);
	LOG_DEBUG("lm75 is %p", lm75);

	double value;
	RESULT_EXCEPTION_ACT(b0_lm75_read(lm75, &value), 0);
	return value;
}

/*
 * Class:     com_madresistor_box0_driver_Lm75
 * Method:    read16
 * Signature: (Ljava/nio/ByteBuffer;)D
 */
JNIEXPORT jdouble JNICALL Java_com_madresistor_box0_driver_Lm75_read16
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_lm75 *lm75 = EXTRACT_LM75(env, obj);
	LOG_DEBUG("lm75 is %p", lm75);

	double value;
	RESULT_EXCEPTION_ACT(b0_lm75_read16(lm75, &value), 0);
	return value;
}
