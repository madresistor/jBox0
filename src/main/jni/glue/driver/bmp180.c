/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_driver_Bmp180.h"
#include "private.h"
#include <libbox0/libbox0.h>
#include <libbox0/driver/bmp180/bmp180.h>

/*
 * Class:     com_madresistor_box0_driver_Bmp180
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_driver_Bmp180_open
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	b0_bmp180 *bmp180;
	RESULT_EXCEPTION_ACT(b0_bmp180_open(i2c, &bmp180), 0);

	LOG_DEBUG("bmp180 is %p", bmp180);
	return (*env)->NewDirectByteBuffer(env, bmp180, 0);
}

/*
 * Class:     com_madresistor_box0_driver_Bmp180
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Bmp180_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_bmp180 *bmp180 = EXTRACT_BMP180(env, obj);
	LOG_DEBUG("bmp180 is %p", bmp180);

	RESULT_EXCEPTION_ACT(b0_bmp180_close(bmp180));
}

/*
 * Class:     com_madresistor_box0_driver_Bmp180
 * Method:    overSampleSet
 * Signature: (Ljava/nio/ByteBuffer;I)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Bmp180_overSampleSet
  (JNIEnv *env, jclass cls, jobject obj, jint value)
{
	UNUSED(cls);
	b0_bmp180 *bmp180 = EXTRACT_BMP180(env, obj);
	LOG_DEBUG("bmp180 is %p", bmp180);

	RESULT_EXCEPTION_ACT(b0_bmp180_over_samp_set(bmp180, value));
}

/*
 * Class:     com_madresistor_box0_driver_Bmp180
 * Method:    read
 * Signature: (Ljava/nio/ByteBuffer;)D
 */
JNIEXPORT jdouble JNICALL Java_com_madresistor_box0_driver_Bmp180_read__Ljava_nio_ByteBuffer_2
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_bmp180 *bmp180 = EXTRACT_BMP180(env, obj);
	LOG_DEBUG("bmp180 is %p", bmp180);

	double value;
	RESULT_EXCEPTION_ACT(b0_bmp180_read(bmp180, &value, NULL), 0);
	return value;
}

/*
 * Class:     com_madresistor_box0_driver_Bmp180
 * Method:    read
 * Signature: (Ljava/nio/ByteBuffer;[D)V
 */
/* note: expecting _values array of minimum length 2 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Bmp180_read__Ljava_nio_ByteBuffer_2_3D
  (JNIEnv *env, jclass cls, jobject obj, jdoubleArray _values)
{
	double values[2];

	UNUSED(cls);
	b0_bmp180 *bmp180 = EXTRACT_BMP180(env, obj);
	LOG_DEBUG("bmp180 is %p", bmp180);

	RESULT_EXCEPTION_ACT(b0_bmp180_read(bmp180, &values[0], &values[1]));

	/* copy back the values */
	(*env)->SetDoubleArrayRegion(env, _values, 0, 2, values);
}
