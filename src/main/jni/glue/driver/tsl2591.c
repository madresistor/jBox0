/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_driver_Tsl2591.h"
#include "private.h"
#include <libbox0/libbox0.h>
#include <libbox0/driver/tsl2591/tsl2591.h>

/*
 * Class:     com_madresistor_box0_driver_Tsl2591
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;Lcom/madresistor/box0/driver/Tsl2591/Config;)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_driver_Tsl2591_open
  (JNIEnv *env, jclass cls, jobject obj, jobject _config)
{
	UNUSED(cls);
	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	struct b0_tsl2591_config config_;
	struct b0_tsl2591_config *config = NULL;
	if (_config) {
		jclass cls_config = (*env)->FindClass (env, "com/madresistor/box0/driver/Tsl2591$Config");
		jfieldID DGF = (*env)->GetFieldID(env, cls_config, "DGF", "D");
		jfieldID CoefB = (*env)->GetFieldID(env, cls_config, "CoefB", "D");
		jfieldID CoefC = (*env)->GetFieldID(env, cls_config, "CoefC", "D");
		jfieldID CoefD = (*env)->GetFieldID(env, cls_config, "CoefD", "D");

		config = &config_;
		config->DGF = (*env)->GetDoubleField(env, _config, DGF);
		config->CoefB = (*env)->GetDoubleField(env, _config, CoefB);
		config->CoefC = (*env)->GetDoubleField(env, _config, CoefC);
		config->CoefD = (*env)->GetDoubleField(env, _config, CoefD);
	}

	b0_tsl2591 *tsl2591;
	RESULT_EXCEPTION_ACT(b0_tsl2591_open(i2c, &tsl2591, config), 0);

	LOG_DEBUG("tsl2591 is %p", tsl2591);
	return (*env)->NewDirectByteBuffer(env, tsl2591, 0);
}

/*
 * Class:     com_madresistor_box0_driver_Tsl2591
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Tsl2591_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_tsl2591 *tsl2591 = EXTRACT_TSL2591(env, obj);
	LOG_DEBUG("tsl2591 is %p", tsl2591);

	RESULT_EXCEPTION_ACT(b0_tsl2591_close(tsl2591));
}

/*
 * Class:     com_madresistor_box0_driver_Tsl2591
 * Method:    read
 * Signature: (Ljava/nio/ByteBuffer;)D
 */
JNIEXPORT jdouble JNICALL Java_com_madresistor_box0_driver_Tsl2591_read
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_tsl2591 *tsl2591 = EXTRACT_TSL2591(env, obj);
	LOG_DEBUG("tsl2591 is %p", tsl2591);

	double value;
	RESULT_EXCEPTION_ACT(b0_tsl2591_read(tsl2591, &value), 0);

	return value;
}

/*
 * Class:     com_madresistor_box0_driver_Tsl2591
 * Method:    integSet
 * Signature: (Ljava/nio/ByteBuffer;I)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Tsl2591_integSet
  (JNIEnv *env, jclass cls, jobject obj, jint value)
{
	UNUSED(cls);
	b0_tsl2591 *tsl2591 = EXTRACT_TSL2591(env, obj);
	LOG_DEBUG("tsl2591 is %p", tsl2591);

	RESULT_EXCEPTION_ACT(b0_tsl2591_integ_set(tsl2591, value));
}

/*
 * Class:     com_madresistor_box0_driver_Tsl2591
 * Method:    gainSet
 * Signature: (Ljava/nio/ByteBuffer;I)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Tsl2591_gainSet
  (JNIEnv *env, jclass cls, jobject obj, jint value)
{
	UNUSED(cls);
	b0_tsl2591 *tsl2591 = EXTRACT_TSL2591(env, obj);
	LOG_DEBUG("tsl2591 is %p", tsl2591);

	RESULT_EXCEPTION_ACT(b0_tsl2591_gain_set(tsl2591, value));
}
