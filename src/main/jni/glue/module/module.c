/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_module_Module.h"
#include "private.h"

/*
 * Class:     com_madresistor_box0_module_Module
 * Method:    getIndex
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Module_getIndex
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_module *mod = EXTRACT_MODULE(env, obj);
	LOG_DEBUG("mod is %p", mod);

	return mod->index;
}

/*
 * Class:     com_madresistor_box0_module_Module
 * Method:    getType
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Module_getType
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_module *mod = EXTRACT_MODULE(env, obj);
	LOG_DEBUG("mod is %p", mod);

	return mod->type;
}

/*
 * Class:     com_madresistor_box0_module_Module
 * Method:    getName
 * Signature: (Ljava/nio/ByteBuffer;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_madresistor_box0_module_Module_getName
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_module *mod = EXTRACT_MODULE(env, obj);
	LOG_DEBUG("mod is %p", mod);

	return (*env)->NewStringUTF(env, (char const *) mod->name);
}

/*
 * Class:     com_madresistor_box0_module_Module
 * Method:    openable
 * Signature: (Ljava/nio/ByteBuffer;)Z
 */
JNIEXPORT jboolean JNICALL Java_com_madresistor_box0_module_Module_openable
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_module *mod = EXTRACT_MODULE(env, obj);
	LOG_DEBUG("mod is %p", mod);

	b0_result_code rc = b0_module_openable(mod);
	if (rc == B0_OK) {
		return JNI_TRUE;
	} else if (rc == B0_ERR_UNAVAIL) {
		return JNI_FALSE;
	}

	RESULT_EXCEPTION_ACT(rc, JNI_FALSE);
	return JNI_FALSE;
}
