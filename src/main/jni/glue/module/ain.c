/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_module_Ain.h"
#include "private.h"
#include <string.h>

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    propertyRef
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Ain/Ref;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Ain_propertyRef
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	return PROPERTY_REF(env, "Ain", ain->ref.low, ain->ref.high, ain->ref.type);
}

static jobjectArray mode_array(JNIEnv *env,
			const struct b0_ain_mode_bitsize_speeds *mode)
{
	size_t i;

	/* Get the class */
	const char *cls_name = "com/madresistor/box0/module/Ain$BitsizeSpeeds";
	jclass cls = (*env)->FindClass(env, cls_name);
	if (cls == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* Get the constructor of BitsizeSpeeds */
	jmethodID init = (*env)->GetMethodID(env, cls, "<init>",
				"(ILcom/madresistor/box0/extra/CUnsignedLongArray;)V");
	if (init == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* Create an array of type BitsizeSpeeds */
	jobjectArray arr = (*env)->NewObjectArray(env, mode->count, cls, JAVA_NULL);
	if (arr == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* Populate the array */
	for (i = 0; i < mode->count; i++) {
		struct b0_ain_bitsize_speeds *bss = &mode->values[i];

		/* Build the contructor argument (speed) */
		jobject speeds = c_unsigned_long_array(env, bss->speed.values, bss->speed.count);
		if (speeds == JAVA_NULL) {
			return JAVA_NULL;
		}

		/* Call the BitsizeSpeeds contructor */
		jobject entry = (*env)->NewObject(env, cls, init, bss->bitsize, speeds);
		if (entry == JAVA_NULL) {
			return JAVA_NULL;
		}

		(*env)->DeleteLocalRef(env, speeds);

		/* Set the value in array */
		(*env)->SetObjectArrayElement(env, arr, i, entry);
		if ((*env)->ExceptionCheck(env)) {
			return JAVA_NULL;
		}

		(*env)->DeleteLocalRef(env, entry);
	}

	(*env)->DeleteLocalRef(env, cls);

	return arr;
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    propertyStream
 * Signature: (Ljava/nio/ByteBuffer;)[Lcom/madresistor/box0/module/Ain/BitsizeSpeeds;
 */
JNIEXPORT jobjectArray JNICALL Java_com_madresistor_box0_module_Ain_propertyStream
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	return mode_array(env, &ain->stream);
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    propertySnapshot
 * Signature: (Ljava/nio/ByteBuffer;)[Lcom/madresistor/box0/module/Ain/BitsizeSpeeds;
 */
JNIEXPORT jobjectArray JNICALL Java_com_madresistor_box0_module_Ain_propertySnapshot
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	return mode_array(env, &ain->snapshot);
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    propertyChanCount
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Ain_propertyChanCount
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	return ain->chan_count;
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    propertyBufferSize
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_com_madresistor_box0_module_Ain_propertyBufferSize
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	return ain->buffer_size;
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    propertyCapab
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Ain_propertyCapab
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	return ain->capab;
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    propertyLabel
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Ain/Label;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Ain_propertyLabel
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	jobjectArray arr = to_java_string_array(env, ain->label.chan,
				ain->chan_count);
	if (arr == JAVA_NULL) {
		return JAVA_NULL;
	}

	return PROPERTY_LABEL(env, "Ain", arr);
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    snapshotPrepare
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Ain_snapshotPrepare
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	RESULT_EXCEPTION_ACT(b0_ain_snapshot_prepare(ain));
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    snapshotStart
 * Signature: (Ljava/nio/ByteBuffer;[F)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Ain_snapshotStart__Ljava_nio_ByteBuffer_2_3F
  (JNIEnv *env, jclass cls, jobject obj, jfloatArray _data)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	jsize count = (*env)->GetArrayLength(env, _data);
	jfloat *data = (*env)->GetFloatArrayElements(env, _data, NULL);
	if (data == NULL) {
		return;
	}

	b0_result_code r = b0_ain_snapshot_start_float(ain, (float *)data, (size_t)count);
	(*env)->ReleaseFloatArrayElements(env, _data, data, 0);
	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    snapshotStart
 * Signature: (Ljava/nio/ByteBuffer;[D)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Ain_snapshotStart__Ljava_nio_ByteBuffer_2_3D
  (JNIEnv *env, jclass cls, jobject obj, jdoubleArray _data)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	jsize count = (*env)->GetArrayLength(env, _data);
	jdouble *data = (*env)->GetDoubleArrayElements(env, _data, NULL);
	if (data == NULL) {
		return;
	}

	b0_result_code r = b0_ain_snapshot_start_double(ain, (double *)data, (size_t)count);
	(*env)->ReleaseDoubleArrayElements(env, _data, data, 0);
	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    snapshotStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Ain_snapshotStop
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	RESULT_EXCEPTION_ACT(b0_ain_snapshot_stop(ain));
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    streamPrepare
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Ain_streamPrepare
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	RESULT_EXCEPTION_ACT(b0_ain_stream_prepare(ain));
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    streamStart
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Ain_streamStart
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	RESULT_EXCEPTION_ACT(b0_ain_stream_start(ain));
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    streamRead
 * Signature: (Ljava/nio/ByteBuffer;[FZ)J
 */
JNIEXPORT jlong JNICALL Java_com_madresistor_box0_module_Ain_streamRead__Ljava_nio_ByteBuffer_2_3FZ
  (JNIEnv *env, jclass cls, jobject obj, jfloatArray _data, jboolean partialRead)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	jfloat *data = (*env)->GetFloatArrayElements(env, _data, NULL);
	if (data == NULL) {
		return 0;
	}

	size_t count = (size_t) (*env)->GetArrayLength(env, _data);
	size_t *ptr_count = BOOL_JAVA_TO_C(partialRead) ? &count : NULL;
	b0_result_code r = b0_ain_stream_read_float(ain, (float *)data, count, ptr_count);
	(*env)->ReleaseFloatArrayElements(env, _data, data, 0);
	RESULT_EXCEPTION_ACT(r, 0);

	return count;
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    streamRead
 * Signature: (Ljava/nio/ByteBuffer;[DZ)J
 */
JNIEXPORT jlong JNICALL Java_com_madresistor_box0_module_Ain_streamRead__Ljava_nio_ByteBuffer_2_3DZ
  (JNIEnv *env, jclass cls, jobject obj, jdoubleArray _data, jboolean partialRead)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	jdouble *data = (*env)->GetDoubleArrayElements(env, _data, NULL);
	if (data == NULL) {
		return 0;
	}

	size_t count = (size_t) (*env)->GetArrayLength(env, _data);
	size_t *ptr_count = BOOL_JAVA_TO_C(partialRead) ? &count : NULL;
	b0_result_code r = b0_ain_stream_read_double(ain, (double *)data, count, ptr_count);
	(*env)->ReleaseDoubleArrayElements(env, _data, data, 0);
	RESULT_EXCEPTION_ACT(r, 0);

	return count;
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    streamStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Ain_streamStop
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	RESULT_EXCEPTION_ACT(b0_ain_stream_stop(ain));
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Ain_open
  (JNIEnv *env, jclass cls, jobject _dev, jint index)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, _dev);
	LOG_DEBUG("dev is %p", dev);

	b0_ain *ain;
	RESULT_EXCEPTION_ACT(b0_ain_open(dev, &ain, index), JAVA_NULL);

	return (*env)->NewDirectByteBuffer(env, ain, 0);
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Ain_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	RESULT_EXCEPTION_ACT(b0_ain_close(ain));
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    setBitsizeSpeed
 * Signature: (Ljava/nio/ByteBuffer;IJ)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Ain_setBitsizeSpeed
  (JNIEnv *env, jclass cls, jobject obj, jint bitsize, jlong speed)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	RESULT_EXCEPTION_ACT(b0_ain_bitsize_speed_set(ain,
		(unsigned int) bitsize, (unsigned long) speed));
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    getBitsizeSpeed
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Ain/BitsizeSpeed;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Ain_getBitsizeSpeed
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	unsigned int bitsize;
	unsigned long speed;
	RESULT_EXCEPTION_ACT(b0_ain_bitsize_speed_get(ain, &bitsize, &speed), 0);

	jclass cls_bs = (*env)->FindClass(env, "com/madresistor/box0/module/Ain$BitsizeSpeed");
	if (cls_bs == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* BitsizeSpeed(int, long) */
	jmethodID init = (*env)->GetMethodID(env, cls_bs, "<init>", "(IJ)V");
	if (init == JAVA_NULL) {
		return JAVA_NULL;
	}

	return (*env)->NewObject(env, cls, init, bitsize, speed);
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    setChanSeq
 * Signature: (Ljava/nio/ByteBuffer;[II)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Ain_setChanSeq
  (JNIEnv *env, jclass cls, jobject obj, jintArray _data, jint len)
{
	b0_result_code r;

	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	jint *data = (*env)->GetIntArrayElements(env, _data, NULL);
	if (data == NULL) {
		return;
	}

	r = b0_ain_chan_seq_set(ain, (unsigned int *) data, (size_t) len);

	(*env)->ReleaseIntArrayElements(env, _data, data, JNI_ABORT);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Ain
 * Method:    getChanSeq
 * Signature: (Ljava/nio/ByteBuffer;)[I
 */
JNIEXPORT jintArray JNICALL Java_com_madresistor_box0_module_Ain_getChanSeq
  (JNIEnv *env, jclass cls, jobject obj)
{
	b0_result_code r;

	UNUSED(cls);

	b0_ain *ain = EXTRACT_AIN(env, obj);
	LOG_DEBUG("ain is %p", ain);

	/* Perform memory allocation */
	size_t alloc_len = ain->chan_count + 1;
	unsigned int *data = NULL, *old_data;

	/* Repeatable point if the full list could not be readed */
	perform:
	data = realloc(old_data = data, sizeof(*data) * alloc_len);
	if (data == NULL) {
		if (old_data != NULL) {
			free(old_data);
		}

		ThrowRuntimeException("realloc returned NULL");
		return JAVA_NULL;
	}

	/* Fetch */
	size_t len = alloc_len;
	r = b0_ain_chan_seq_get(ain, data, &len);
	if (B0_ERR_RC(r)) {
		free(data);
		RESULT_EXCEPTION_ACT(r, JAVA_NULL);
	} else if (len == alloc_len) {
		alloc_len *= 2;
		goto perform;
	}

	/* Create array */
	jintArray arr = (*env)->NewIntArray(env, (jsize) len);
	if (arr == JAVA_NULL) {
		goto done;
	}

	/* Copy to array */
	(*env)->SetIntArrayRegion(env, arr, 0, len, (jint *) data);

	done:
	/* Free the allocate memory */
	free(data);

	return arr;
}
