/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_module_Spi.h"
#include "private.h"

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    propertySsCount
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Spi_propertySsCount
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	return spi->ss_count;
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    propertyRef
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Spi/Ref;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Spi_propertyRef
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	return PROPERTY_REF(env, "Spi", spi->ref.low, spi->ref.high, spi->ref.type);
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    propertyBitsize
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/extra/CUnsignedIntArray;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Spi_propertyBitsize
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	return c_unsigned_int_array(env, spi->bitsize.values, spi->bitsize.count);
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    propertySpeed
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/extra/CUnsignedLongArray;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Spi_propertySpeed
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	return c_unsigned_long_array(env, spi->speed.values, spi->speed.count);
}

static jobject property_label(JNIEnv *env, b0_spi *spi)
{
	jclass cls = (*env)->FindClass(env, "com/madresistor/box0/module/Spi$Label");
	if (cls == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* Label(String, String, String, String[]) */
	const char *init_sign =
		"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V";
	jmethodID init = (*env)->GetMethodID(env, cls, "<init>", init_sign);
	if (init == JAVA_NULL) {
		return JAVA_NULL;
	}

	jobjectArray ss = to_java_string_array(env, spi->label.ss, spi->ss_count);
	if (ss == JAVA_NULL) {
		return JAVA_NULL;
	}

	jstring sclk = (*env)->NewStringUTF(env, (char const *) spi->label.sclk);
	if (sclk == JAVA_NULL) {
		return JAVA_NULL;
	}

	jstring mosi = (*env)->NewStringUTF(env, (char const *) spi->label.mosi);
	if (mosi == JAVA_NULL) {
		return JAVA_NULL;
	}

	jstring miso = (*env)->NewStringUTF(env, (char const *) spi->label.miso);
	if (miso == JAVA_NULL) {
		return JAVA_NULL;
	}

	return (*env)->NewObject(env, cls, init, sclk, mosi, miso, ss);
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    propertyLabel
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Spi/Label;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Spi_propertyLabel
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	return property_label(env, spi);
}

static struct {
	jfieldID flags, addr, speed, bitsize, rdata, wdata, count;
} task_fields;

bool spi_cache_task_field(JNIEnv *env)
{
	const char *clz_name = "com/madresistor/box0/module/Spi$Task";
	jclass clz = (*env)->FindClass(env, clz_name);

	task_fields.flags = (*env)->GetFieldID(env, clz, "flags", "I");
	if (task_fields.flags == JAVA_NULL) {
		LOG_DEBUG("unable to get Spi$Task.flags field ID");
		return false;
	}

	task_fields.addr = (*env)->GetFieldID(env, clz, "addr", "I");
	if (task_fields.addr == JAVA_NULL) {
		LOG_DEBUG("unable to get Spi$Task.addr field ID");
		return false;
	}

	task_fields.speed = (*env)->GetFieldID(env, clz, "speed", "J");
	if (task_fields.speed == JAVA_NULL) {
		LOG_DEBUG("unable to get Spi$Task.speed field ID");
		return false;
	}

	task_fields.bitsize = (*env)->GetFieldID(env, clz, "bitsize", "I");
	if (task_fields.bitsize == JAVA_NULL) {
		LOG_DEBUG("unable to get Spi$Task.addr field ID");
		return false;
	}

	task_fields.wdata = (*env)->GetFieldID(env, clz, "wdata", "[B");
	if (task_fields.wdata == JAVA_NULL) {
		LOG_DEBUG("unable to get Spi$Task.wdata field ID");
		return false;
	}

	task_fields.rdata = (*env)->GetFieldID(env, clz, "rdata", "[B");
	if (task_fields.rdata == JAVA_NULL) {
		LOG_DEBUG("unable to get Spi$Task.rdata field ID");
		return false;
	}

	task_fields.count = (*env)->GetFieldID(env, clz, "count", "I");
	if (task_fields.count == JAVA_NULL) {
		LOG_DEBUG("unable to get Spi$Task.count field ID");
		return false;
	}

	return true;
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    masterPrepare
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Spi_masterPrepare
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	RESULT_EXCEPTION_ACT(b0_spi_master_prepare(spi));
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    masterStart
 * Signature: (Ljava/nio/ByteBuffer;[Lcom/madresistor/box0/module/Spi/Task;[I)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Spi_masterStart
  (JNIEnv *env, jclass cls, jobject obj, jobjectArray _tasks, jintArray _failed_task)
{
	int i, j;
	size_t k;

	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	/* Check if the task array is actually useful */
	int tasks_count = (*env)->GetArrayLength(env, _tasks);
	ENSURE_GREATER_THAN_ZERO(env, tasks_count);

	/* Allocate memory for task */
	b0_spi_task *tasks = malloc(sizeof(*tasks) * tasks_count);

	/* Keep a list of java array and its respective data.
	 *  mode is "0" (write back) if read
	 *  mode is "JNI_ABORT" if write (since there is nothing to write back)
	 */
	struct task_mem_item {
		jobject array;
		jbyte *data;
		jint mode;
	};

	/* The number of memories to keep reference
	 *  can be of maximum 2 * "tasks_count" */
	struct task_mem_item *mem_list = malloc(sizeof(*mem_list) * 2 * tasks_count);
	size_t mem_list_used = 0;
	bool conv_failed = false; /* set to true if the loop fails */

	/* Convert the Java task array to C task array */
	for (i = 0; i < tasks_count; i++) {
		jobject _task = (*env)->GetObjectArrayElement(env, _tasks, i);
		b0_spi_task *task = &tasks[i];

		task->flags = (*env)->GetIntField(env, _task, task_fields.flags);
		task->addr = (*env)->GetIntField(env, _task, task_fields.addr);
		task->bitsize = (*env)->GetIntField(env, _task, task_fields.bitsize);
		task->speed = (*env)->GetLongField(env, _task, task_fields.speed);
		task->count = (*env)->GetIntField(env, _task, task_fields.count);
		task->wdata = NULL;
		task->rdata = NULL;

		/* We need to provide data? */
		if (task->count <= 0) {
			continue;
		}

		/* Get the item in which the java array will be referenced */
		for (j = 0; j < 2; j++) {
			bool read = !j;

			struct task_mem_item *mem = &mem_list[mem_list_used];
			mem->array = (*env)->GetObjectField(env, _task,
				read ? task_fields.rdata : task_fields.wdata);

			if ((*env)->ExceptionCheck(env)) {
				conv_failed = true;
				goto out;
			}

			if (mem->array == JAVA_NULL) {
				/* this pointer is NULL */
				continue;
			}

			/* Make sure the number of bytes we are expecting from the java byte
			 *  array is there.
			 * make sure array.length >= count
			 */
			int len = (*env)->GetArrayLength(env, mem->array);

			if ((*env)->ExceptionCheck(env)) {
				conv_failed = true;
				goto out;
			}

			if (task->count > (unsigned)len) {
				LOG_DEBUG("Task %i %s array data length less than count",
					i, j ? "Write" : "Read");
				conv_failed = true;
				goto out;
			}

			/* Get data from the array */
			mem->data = (*env)->GetByteArrayElements(env, mem->array, NULL);

			if ((*env)->ExceptionCheck(env)) {
				conv_failed = true;
				goto out;
			}

			if (mem->array == JAVA_NULL) {
				LOG_DEBUG("Task %i %s got data array NULL",
					i, j ? "Write" : "Read");
				conv_failed = true;
				goto out;
			}

			/* finally update the list */
			mem->mode = read ? 0 : JNI_ABORT;
			if (read) {
				task->rdata = mem->data;
			} else {
				task->wdata = mem->data;
			}
			mem_list_used++;
		}
	}

	out:

	/* loop (converter) failed? */
	if (conv_failed) {
		goto release_memory;
	}

	b0_result_code r;

	/* Extract the memory where we need to
	 *  keep the failed task index and count */
	jint *failed_task = (*env)->GetIntArrayElements(env, _failed_task, NULL);
	if ((*env)->ExceptionCheck(env)) {
		conv_failed = true;
		goto release_memory;
	}

	if (failed_task == NULL) {
		conv_failed = true;
		LOG_DEBUG("failed_task array is NULL");
		goto release_memory;
	}

	/* Execute the tasks */
	r = b0_spi_master_start(spi, tasks, &failed_task[0], &failed_task[1]);

	/* Release the failed_task array */
	(*env)->ReleaseIntArrayElements(env, _failed_task, failed_task, 0);

	release_memory:

	/* Release the java array */
	for (k = 0; k < mem_list_used; k++) {
		struct task_mem_item *mem = &mem_list[k];
		(*env)->ReleaseByteArrayElements(env, mem->array, mem->data, mem->mode);
	}

	/* Free up all intermediate memories */
	free(mem_list);
	free(tasks);

	if (!conv_failed) {
		RESULT_EXCEPTION_ACT(r);
	}
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    masterStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Spi_masterStop
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);
	RESULT_EXCEPTION_ACT(b0_spi_master_stop(spi));
}

static struct {
	jfieldID addr, flags, speed, bitsize;
} sugar_arg_fields;

bool spi_cache_sugar_arg_field(JNIEnv *env)
{
	const char *clz_name = "com/madresistor/box0/module/Spi$SugarArg";
	jclass clz = (*env)->FindClass(env, clz_name);

	sugar_arg_fields.addr = (*env)->GetFieldID(env, clz, "addr", "I");
	if ((*env)->ExceptionCheck(env)) {
		LOG_DEBUG("unable to get Spi$SugarArg.addr field ID");
		return false;
	}

	sugar_arg_fields.flags = (*env)->GetFieldID(env, clz, "flags", "I");
	if ((*env)->ExceptionCheck(env)) {
		LOG_DEBUG("unable to get Spi$SugarArg.flags field ID");
		return false;
	}

	sugar_arg_fields.speed = (*env)->GetFieldID(env, clz, "speed", "J");
	if ((*env)->ExceptionCheck(env)) {
		LOG_DEBUG("unable to get Spi$SugarArg.speed field ID");
		return false;
	}

	sugar_arg_fields.bitsize = (*env)->GetFieldID(env, clz, "bitsize", "I");
	if ((*env)->ExceptionCheck(env)) {
		LOG_DEBUG("unable to get Spi$SugarArg.bitsize field ID");
		return false;
	}

	return true;
}

static bool extract_sugar_arg(JNIEnv *env, jobject obj, b0_spi_sugar_arg *arg)
{
	arg->addr = (*env)->GetIntField(env, obj, sugar_arg_fields.addr);
	if ((*env)->ExceptionCheck(env)) {
		return false;
	}

	arg->flags = (*env)->GetIntField(env, obj, sugar_arg_fields.flags);
	if ((*env)->ExceptionCheck(env)) {
		return false;
	}

	arg->speed = (*env)->GetLongField(env, obj, sugar_arg_fields.speed);
	if ((*env)->ExceptionCheck(env)) {
		return false;
	}

	arg->bitsize = (*env)->GetIntField(env, obj, sugar_arg_fields.bitsize);
	if ((*env)->ExceptionCheck(env)) {
		return false;
	}

	return true;
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    masterFd
 * Signature: (Ljava/nio/ByteBuffer;Lcom/madresistor/box0/module/Spi/SugarArg;[B[BI)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Spi_masterFd
  (JNIEnv *env, jclass cls, jobject obj, jobject _arg, jbyteArray _write,
		jbyteArray _read, jint count)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	b0_spi_sugar_arg arg;
	if (!extract_sugar_arg(env, _arg, &arg)) {
		return;
	}

	void *write = (*env)->GetByteArrayElements(env, _write, NULL);
	if (write == NULL) {
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	void *read = (*env)->GetByteArrayElements(env, _read, NULL);
	if (read == NULL) {
		(*env)->ReleaseByteArrayElements(env, _read, read, JNI_ABORT);
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	b0_result_code r = b0_spi_master_fd(spi, &arg, write, read, count);

	(*env)->ReleaseByteArrayElements(env, _write, write, JNI_ABORT);
	(*env)->ReleaseByteArrayElements(env, _read, read, 0);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    masterHdRead
 * Signature: (Ljava/nio/ByteBuffer;Lcom/madresistor/box0/module/Spi/SugarArg;[BI)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Spi_masterHdRead
  (JNIEnv *env, jclass cls, jobject obj, jobject _arg, jbyteArray _data, jint count)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	b0_spi_sugar_arg arg;
	if (!extract_sugar_arg(env, _arg, &arg)) {
		return;
	}

	ENSURE_GREATER_THAN_ZERO(env, count);
	ENSURE_ATLEAST_LENGTH_OF_ARRAY(env, _data, count);

	void *data = (*env)->GetByteArrayElements(env, _data, NULL);
	if (data == NULL) {
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	b0_result_code r = b0_spi_master_hd_read(spi, &arg, data, count);

	(*env)->ReleaseByteArrayElements(env, _data, data, 0);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    masterHdWrite
 * Signature: (Ljava/nio/ByteBuffer;Lcom/madresistor/box0/module/Spi/SugarArg;[BI)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Spi_masterHdWrite
  (JNIEnv *env, jclass cls, jobject obj, jobject _arg, jbyteArray _data, jint count)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	b0_spi_sugar_arg arg;
	if (!extract_sugar_arg(env, _arg, &arg)) {
		return;
	}

	ENSURE_GREATER_THAN_ZERO(env, count);
	ENSURE_ATLEAST_LENGTH_OF_ARRAY(env, _data, count);

	void *data = (*env)->GetByteArrayElements(env, _data, NULL);
	if (data == NULL) {
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	b0_result_code r = b0_spi_master_hd_write(spi, &arg, data, count);

	(*env)->ReleaseByteArrayElements(env, _data, data, JNI_ABORT);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    masterHdWriteRead
 * Signature: (Ljava/nio/ByteBuffer;Lcom/madresistor/box0/module/Spi/SugarArg;[BI[BI)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Spi_masterHdWriteRead
  (JNIEnv *env, jclass cls, jobject obj, jobject _arg, jbyteArray _write_data,
		jint write_count, jbyteArray _read_data, jint read_count)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	b0_spi_sugar_arg arg;
	if (!extract_sugar_arg(env, _arg, &arg)) {
		return;
	}

	ENSURE_GREATER_THAN_ZERO(env, write_count);
	ENSURE_ATLEAST_LENGTH_OF_ARRAY(env, _write_data, write_count);

	ENSURE_GREATER_THAN_ZERO(env, read_count);
	ENSURE_ATLEAST_LENGTH_OF_ARRAY(env, _read_data, read_count);

	void *write_data = (*env)->GetByteArrayElements(env, _write_data, NULL);
	if (write_data == NULL) {
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	void *read_data = (*env)->GetByteArrayElements(env, _read_data, NULL);
	if (read_data == NULL) {
		(*env)->ReleaseByteArrayElements(env, _write_data, write_data, JNI_ABORT);
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	b0_result_code r = b0_spi_master_hd_write_read(spi, &arg, write_data,
		write_count, read_data, read_count);

	(*env)->ReleaseByteArrayElements(env, _write_data, write_data, JNI_ABORT);
	(*env)->ReleaseByteArrayElements(env, _read_data, read_data, 0);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Spi_open
  (JNIEnv *env, jclass cls, jobject _dev, jint index)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, _dev);
	LOG_DEBUG("dev is %p", dev);

	b0_spi *spi;
	RESULT_EXCEPTION_ACT(b0_spi_open(dev, &spi, index), 0);

	return (*env)->NewDirectByteBuffer(env, spi, 0);
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Spi_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	RESULT_EXCEPTION_ACT(b0_spi_close(spi));
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    setActiveState
 * Signature: (Ljava/nio/ByteBuffer;IZ)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Spi_setActiveState
  (JNIEnv *env, jclass cls, jobject obj, jint addr, jboolean value)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	RESULT_EXCEPTION_ACT(b0_spi_active_state_set(spi,
		(unsigned int) addr, BOOL_JAVA_TO_C(value)));
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    getActiveState
 * Signature: (Ljava/nio/ByteBuffer;I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_madresistor_box0_module_Spi_getActiveState
  (JNIEnv *env, jclass cls, jobject obj, jint addr)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	bool value;
	RESULT_EXCEPTION_ACT(b0_spi_active_state_get(spi, (unsigned int) addr,
		&value), JNI_FALSE);
	return BOOL_C_TO_JAVA(value);
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    setSpeed
 * Signature: (Ljava/nio/ByteBuffer;J)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Spi_setSpeed
  (JNIEnv *env, jclass cls, jobject obj, jlong value)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	RESULT_EXCEPTION_ACT(b0_spi_speed_set(spi, (unsigned long) value));
}

/*
 * Class:     com_madresistor_box0_module_Spi
 * Method:    getSpeed
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_com_madresistor_box0_module_Spi_getSpeed
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_spi *spi = EXTRACT_SPI(env, obj);
	LOG_DEBUG("spi is %p", spi);

	unsigned long value;
	RESULT_EXCEPTION_ACT(b0_spi_speed_get(spi, &value), 0);
	return value;
}
