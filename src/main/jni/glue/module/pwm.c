/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_module_Pwm.h"
#include "private.h"

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    propertyRef
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Pwm/Ref;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Pwm_propertyRef
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	return PROPERTY_REF(env, "Pwm", pwm->ref.low, pwm->ref.high, pwm->ref.type);
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    propertyBitsize
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/extra/CUnsignedIntArray;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Pwm_propertyBitsize
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	return c_unsigned_int_array(env, pwm->bitsize.values, pwm->bitsize.count);
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    propertySpeed
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/extra/CUnsignedLongArray;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Pwm_propertySpeed
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	return c_unsigned_long_array(env, pwm->speed.values, pwm->speed.count);
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    propertyLabel
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Pwm/Label;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Pwm_propertyLabel
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	jobjectArray arr = to_java_string_array(env, pwm->label.pin,
				pwm->pin_count);
	if (arr == JAVA_NULL) {
		return JAVA_NULL;
	}

	return PROPERTY_LABEL(env, "Pwm", arr);
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    propertyPinCount
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Pwm_propertyPinCount
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	return pwm->pin_count;
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    setPeriod
 * Signature: (Ljava/nio/ByteBuffer;J)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Pwm_setPeriod
  (JNIEnv *env, jclass cls, jobject obj, jlong value)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	RESULT_EXCEPTION_ACT(b0_pwm_period_set(pwm, (b0_pwm_reg) value));
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    getPeriod
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_com_madresistor_box0_module_Pwm_getPeriod
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	b0_pwm_reg value;
	RESULT_EXCEPTION_ACT(b0_pwm_period_get(pwm, &value), 0);

	return (jlong)value;
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    setWidth
 * Signature: (Ljava/nio/ByteBuffer;IJ)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Pwm_setWidth
  (JNIEnv *env, jclass cls, jobject obj, jint index, jlong value)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	RESULT_EXCEPTION_ACT(b0_pwm_width_set(pwm, (unsigned int) index,
			(b0_pwm_reg) value));
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    getWidth
 * Signature: (Ljava/nio/ByteBuffer;I)J
 */
JNIEXPORT jlong JNICALL Java_com_madresistor_box0_module_Pwm_getWidth
  (JNIEnv *env, jclass cls, jobject obj, jint index)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	b0_pwm_reg value;
	RESULT_EXCEPTION_ACT(b0_pwm_width_get(pwm, (unsigned int) index, &value), 0);

	return (jlong)value;
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    outputPrepare
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Pwm_outputPrepare
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	RESULT_EXCEPTION_ACT(b0_pwm_output_prepare(pwm));
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    outputStart
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Pwm_outputStart
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	RESULT_EXCEPTION_ACT(b0_pwm_output_start(pwm));
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    outputStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Pwm_outputStop
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	RESULT_EXCEPTION_ACT(b0_pwm_output_stop(pwm));
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Pwm_open
  (JNIEnv *env, jclass cls, jobject _dev, jint index)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, _dev);
	LOG_DEBUG("dev is %p", dev);

	b0_pwm *pwm;
	RESULT_EXCEPTION_ACT(b0_pwm_open(dev, &pwm, index), 0);

	return (*env)->NewDirectByteBuffer(env, pwm, 0);
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Pwm_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	RESULT_EXCEPTION_ACT(b0_pwm_close(pwm));
}

/* Construct a Pwm.OutputCalc */
static jobject output_calc_object(JNIEnv *env, unsigned long speed,
			b0_pwm_reg period)
{
	static const char *clsName = "com/madresistor/box0/module/Pwm$OutputCalc";
	jclass exClass = (*env)->FindClass (env, clsName);
	jmethodID constructor = (*env)->GetMethodID (env, exClass, "<init>", "(JJ)V");
	jlong arg0 = (jlong) speed;
	jlong arg1 = (jlong) period;
	return (*env)->NewObject(env, exClass, constructor, arg0, arg1);
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    outputCalc
 * Signature: (Ljava/nio/ByteBuffer;IDDZ)Lcom/madresistor/box0/module/Pwm/OutputCalc;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Pwm_outputCalc
  (JNIEnv *env, jclass cls, jobject obj, jint bitsize, jdouble freq,
		jdouble max_error, jboolean best_result)
{
	UNUSED(cls);
	unsigned long speed;
	b0_pwm_reg period;

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	RESULT_EXCEPTION_ACT(b0_pwm_output_calc(pwm, bitsize, freq,
			&speed, &period, max_error, BOOL_JAVA_TO_C(best_result)), 0);

	return output_calc_object(env, speed, period);
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    setBitsize
 * Signature: (Ljava/nio/ByteBuffer;I)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Pwm_setBitsize
  (JNIEnv *env, jclass cls, jobject obj, jint value)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	RESULT_EXCEPTION_ACT(b0_pwm_bitsize_set(pwm, (unsigned int) value));
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    getBitsize
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Pwm_getBitsize
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	unsigned int value;
	RESULT_EXCEPTION_ACT(b0_pwm_bitsize_get(pwm, &value), 0);
	return value;
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    setSpeed
 * Signature: (Ljava/nio/ByteBuffer;J)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Pwm_setSpeed
  (JNIEnv *env, jclass cls, jobject obj, jlong value)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	RESULT_EXCEPTION_ACT(b0_pwm_speed_set(pwm, (unsigned long) value));
}

/*
 * Class:     com_madresistor_box0_module_Pwm
 * Method:    getSpeed
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_com_madresistor_box0_module_Pwm_getSpeed
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_pwm *pwm = EXTRACT_PWM(env, obj);
	LOG_DEBUG("pwm is %p", pwm);

	unsigned long value;
	RESULT_EXCEPTION_ACT(b0_pwm_speed_get(pwm, &value), 0);
	return value;
}
