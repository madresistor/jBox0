/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_module_I2c.h"
#include "private.h"

static jobject property_ref(JNIEnv *env, b0_i2c *i2c)
{
	const char *cls_name = "com/madresistor/box0/module/I2c$Ref";
	jclass cls = (*env)->FindClass(env, cls_name);
	if (cls == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* Ref(double, double) */
	jmethodID init = (*env)->GetMethodID(env, cls, "<init>", "(DD)V");
	if ((*env)->ExceptionCheck(env)) {
		return JAVA_NULL;
	}

	return (*env)->NewObject(env, cls, init, i2c->ref.low, i2c->ref.high);
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    propertyRef
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/I2c/Ref;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_I2c_propertyRef
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	return property_ref(env, i2c);
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    propertyVersion
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/extra/CUnsignedIntArray;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_I2c_propertyVersion
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	return c_unsigned_int_array(env, i2c->version.values, i2c->version.count);
}

static jobject property_label(JNIEnv *env, b0_i2c *i2c)
{
	const char *cls_name = "com/madresistor/box0/module/I2c$Label";
	jclass cls = (*env)->FindClass(env, cls_name);
	if (cls == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* Label(String, String) */
	jmethodID init = (*env)->GetMethodID(env, cls, "<init>",
			"(Ljava/lang/String;Ljava/lang/String;)V");
	if (init == JAVA_NULL) {
		return JAVA_NULL;
	}

	jstring sck = (*env)->NewStringUTF(env, (char const *) i2c->label.sck);
	if (sck == JAVA_NULL) {
		return JAVA_NULL;
	}

	jstring sda = (*env)->NewStringUTF(env, (char const *) i2c->label.sda);
	if (sda == JAVA_NULL) {
		return JAVA_NULL;
	}

	return (*env)->NewObject(env, cls, init, sck, sda);
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    propertyLabel
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/I2c/Label;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_I2c_propertyLabel
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	return property_label(env, i2c);
}

static struct {
	jfieldID flags, addr, data, count;
} task_fields;

bool i2c_cache_task_field(JNIEnv *env)
{
	const char *clz_name = "com/madresistor/box0/module/I2c$Task";
	jclass clz = (*env)->FindClass(env, clz_name);

	task_fields.flags = (*env)->GetFieldID(env, clz, "flags", "I");
	if ((*env)->ExceptionCheck(env)) {
		LOG_DEBUG("unable to get I2c$Task.flags field ID");
		return false;
	}

	task_fields.addr = (*env)->GetFieldID(env, clz, "addr", "B");
	if ((*env)->ExceptionCheck(env)) {
		LOG_DEBUG("unable to get I2c$Task.addr field ID");
		return false;
	}

	task_fields.data = (*env)->GetFieldID(env, clz, "data", "[B");
	if ((*env)->ExceptionCheck(env)) {
		LOG_DEBUG("unable to get I2c$Task.data field ID");
		return false;
	}

	task_fields.count = (*env)->GetFieldID(env, clz, "count", "I");
	if ((*env)->ExceptionCheck(env)) {
		LOG_DEBUG("unable to get I2c$Task.count field ID");
		return false;
	}

	return true;
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    masterPrepare
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_I2c_masterPrepare
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	RESULT_EXCEPTION_ACT(b0_i2c_master_prepare(i2c));
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    masterStart
 * Signature: (Ljava/nio/ByteBuffer;[Lcom/madresistor/box0/module/I2c/Task;[I)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_I2c_masterStart
  (JNIEnv *env, jclass cls, jobject obj, jobjectArray _tasks, jintArray _failed_task)
{
	int i;
	size_t j;

	UNUSED(cls);

	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	/* Check if the task array is actually useful */
	int tasks_count = (*env)->GetArrayLength(env, _tasks);
	ENSURE_GREATER_THAN_ZERO(env, tasks_count);

	/* Allocate memory for task */
	b0_i2c_task *tasks = malloc(sizeof(*tasks) * tasks_count);

	/* Keep a list of java array and its respective data.
	 *  mode is "0" (write back) if read
	 *  mode is "JNI_ABORT" if write (since there is nothing to write back)
	 */
	struct task_mem_item {
		jobject array;
		jbyte *data;
		jint mode;
	};

	/* The number of memories to keep reference can be of maximum "tasks_count" */
	struct task_mem_item *mem_list = malloc(sizeof(*mem_list) * tasks_count);
	size_t mem_list_used = 0;
	bool conv_failed = false; /* set to true if the loop fails */

	/* Convert the Java task array to C task array */
	for (i = 0; i < tasks_count; i++) {
		jobject _task = (*env)->GetObjectArrayElement(env, _tasks, i);
		b0_i2c_task *task = &tasks[i];

		task->flags = (*env)->GetIntField(env, _task, task_fields.flags);
		task->addr = (*env)->GetByteField(env, _task, task_fields.addr);
		task->count = (*env)->GetByteField(env, _task, task_fields.count);
		task->data = NULL;

		/* We need to provide data? */
		if (task->count <= 0) {
			continue;
		}

		/* Get the item in which the java array will be referenced */
		struct task_mem_item *mem = &mem_list[mem_list_used];
		mem->array = (*env)->GetObjectField(env, _task, task_fields.data);

		if ((*env)->ExceptionCheck(env)) {
			conv_failed = true;
			break;
		}

		if (mem->array == JAVA_NULL) {
			conv_failed = true;
			break;
		}

		/* Make sure the number of bytes we are expecting from the java byte
		 *  array is there.
		 * make sure array.length >= count
		 */
		int len = (*env)->GetArrayLength(env, mem->array);

		if ((*env)->ExceptionCheck(env)) {
			conv_failed = true;
			break;
		}

		if (task->count > (unsigned)len) {
			LOG_DEBUG("Task %i array data length less than count", i);
			conv_failed = true;
			break;
		}

		/* Get data from the array */
		mem->data = (*env)->GetByteArrayElements(env, mem->array, NULL);

		if ((*env)->ExceptionCheck(env)) {
			conv_failed = true;
			break;
		}

		if (mem->array == JAVA_NULL) {
			LOG_DEBUG("Task %i got data array NULL", i);
			conv_failed = true;
			break;
		}

		/* finally update the list */
		bool read = (task->flags & B0_I2C_TASK_DIR_MASK) == B0_I2C_TASK_READ;
		mem->mode = read ? 0 : JNI_ABORT;
		mem_list_used++;
		task->data = mem->data;
	}

	/* loop (converter) failed? */
	if (conv_failed) {
		goto release_memory;
	}

	b0_result_code r;

	/* Extract the memory where we need to
	 *  keep the failed task index and ack */
	jint *failed_task = (*env)->GetIntArrayElements(env, _failed_task, NULL);
	if ((*env)->ExceptionCheck(env)) {
		conv_failed = true;
		goto release_memory;
	}

	if (failed_task == NULL) {
		conv_failed = true;
		LOG_DEBUG("failed_task array is NULL");
		goto release_memory;
	}

	/* Execute the tasks */
	r = b0_i2c_master_start(i2c, tasks, &failed_task[0], &failed_task[1]);

	/* Release the failed_task array */
	(*env)->ReleaseIntArrayElements(env, _failed_task, failed_task, 0);

	release_memory:

	/* Release the java array */
	for (j = 0; j < mem_list_used; j++) {
		struct task_mem_item *mem = &mem_list[j];
		(*env)->ReleaseByteArrayElements(env, mem->array, mem->data, mem->mode);
	}

	/* Free up all intermediate memories */
	free(mem_list);
	free(tasks);

	if (!conv_failed) {
		RESULT_EXCEPTION_ACT(r);
	}
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    masterStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_I2c_masterStop
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);
	RESULT_EXCEPTION_ACT(b0_i2c_master_stop(i2c));
}

static struct {
	jfieldID addr, version;
} sugar_arg_fields;

bool i2c_cache_sugar_arg_field(JNIEnv *env)
{
	const char *clz_name = "com/madresistor/box0/module/I2c$SugarArg";
	jclass clz = (*env)->FindClass(env, clz_name);

	sugar_arg_fields.addr = (*env)->GetFieldID(env, clz, "addr", "B");
	if ((*env)->ExceptionCheck(env)) {
		LOG_DEBUG("unable to get I2c$SugarArg.addr field ID");
		return false;
	}

	sugar_arg_fields.version = (*env)->GetFieldID(env, clz, "version", "I");
	if ((*env)->ExceptionCheck(env)) {
		LOG_DEBUG("unable to get I2c$SugarArg.bitsize field ID");
		return false;
	}

	return true;
}

bool extract_sugar_arg(JNIEnv *env, jobject obj, b0_i2c_sugar_arg *arg)
{
	arg->addr = (*env)->GetByteField(env, obj, sugar_arg_fields.addr);
	if ((*env)->ExceptionCheck(env)) {
		return false;
	}

	arg->version = (*env)->GetIntField(env, obj, sugar_arg_fields.version);
	if ((*env)->ExceptionCheck(env)) {
		return false;
	}

	return true;
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    masterRead
 * Signature: (Ljava/nio/ByteBuffer;Lcom/madresistor/box0/module/I2c/SugarArg;[BI)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_I2c_masterRead
  (JNIEnv *env, jclass cls, jobject obj, jobject _arg, jbyteArray _data, jint count)
{
	UNUSED(cls);

	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	b0_i2c_sugar_arg arg;
	if (!extract_sugar_arg(env, _arg, &arg)) {
		return;
	}

	ENSURE_GREATER_THAN_ZERO(env, count);
	ENSURE_ATLEAST_LENGTH_OF_ARRAY(env, _data, count);

	void *data = (*env)->GetByteArrayElements(env, _data, NULL);
	if (data == NULL) {
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	b0_result_code r = b0_i2c_master_read(i2c, &arg, data, count);

	(*env)->ReleaseByteArrayElements(env, _data, data, 0);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    masterWrite
 * Signature: (Ljava/nio/ByteBuffer;Lcom/madresistor/box0/module/I2c/SugarArg;[BI)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_I2c_masterWrite
  (JNIEnv *env, jclass cls, jobject obj, jobject _arg, jbyteArray _data, jint count)
{
	UNUSED(cls);

	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	b0_i2c_sugar_arg arg;
	if (!extract_sugar_arg(env, _arg, &arg)) {
		return;
	}

	ENSURE_GREATER_THAN_ZERO(env, count);
	ENSURE_ATLEAST_LENGTH_OF_ARRAY(env, _data, count);

	void *data = (*env)->GetByteArrayElements(env, _data, NULL);
	if (data == NULL) {
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	b0_result_code r = b0_i2c_master_write(i2c, &arg, data, count);

	(*env)->ReleaseByteArrayElements(env, _data, data, JNI_ABORT);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    masterWriteRead
 * Signature: (Ljava/nio/ByteBuffer;Lcom/madresistor/box0/module/I2c/SugarArg;[BI[BI)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_I2c_masterWriteRead
  (JNIEnv *env, jclass cls, jobject obj, jobject _arg, jbyteArray _write_data,
		jint write_count, jbyteArray _read_data, jint read_count)
{
	UNUSED(cls);

	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	b0_i2c_sugar_arg arg;
	if (!extract_sugar_arg(env, _arg, &arg)) {
		return;
	}

	ENSURE_GREATER_THAN_ZERO(env, write_count);
	ENSURE_ATLEAST_LENGTH_OF_ARRAY(env, _write_data, write_count);

	ENSURE_GREATER_THAN_ZERO(env, read_count);
	ENSURE_ATLEAST_LENGTH_OF_ARRAY(env, _read_data, read_count);

	void *write_data = (*env)->GetByteArrayElements(env, _write_data, NULL);
	if (write_data == NULL) {
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	void *read_data = (*env)->GetByteArrayElements(env, _read_data, NULL);
	if (read_data == NULL) {
		(*env)->ReleaseByteArrayElements(env, _write_data, write_data, JNI_ABORT);
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	b0_result_code r = b0_i2c_master_write_read(i2c, &arg, write_data,
		write_count, read_data, read_count);

	(*env)->ReleaseByteArrayElements(env, _write_data, write_data, JNI_ABORT);
	(*env)->ReleaseByteArrayElements(env, _read_data, read_data, 0);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    masterWrite8Read
 * Signature: (Ljava/nio/ByteBuffer;Lcom/madresistor/box0/module/I2c/SugarArg;B[BI)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_I2c_masterWrite8Read
  (JNIEnv *env, jclass cls, jobject obj, jobject _arg, jbyte write,
		jbyteArray _read_data, jint read_count)
{
	UNUSED(cls);

	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	b0_i2c_sugar_arg arg;
	if (!extract_sugar_arg(env, _arg, &arg)) {
		return;
	}

	ENSURE_GREATER_THAN_ZERO(env, read_count);
	ENSURE_ATLEAST_LENGTH_OF_ARRAY(env, _read_data, read_count);

	void *read_data = (*env)->GetByteArrayElements(env, _read_data, NULL);
	if (read_data == NULL) {
		(*env)->ReleaseByteArrayElements(env, _read_data, read_data, JNI_ABORT);
		LOG_DEBUG("GetByteArrayElements returned NULL");
		return;
	}

	b0_result_code r = b0_i2c_master_write8_read(i2c, &arg,
		(uint8_t) write, read_data, read_count);

	(*env)->ReleaseByteArrayElements(env, _read_data, read_data, 0);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_I2c_open
  (JNIEnv *env, jclass cls, jobject _dev, jint index)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, _dev);
	LOG_DEBUG("dev is %p", dev);

	b0_i2c *i2c;
	RESULT_EXCEPTION_ACT(b0_i2c_open(dev, &i2c, index), 0);

	return (*env)->NewDirectByteBuffer(env, i2c, 0);
}

/*
 * Class:     com_madresistor_box0_module_I2c
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_I2c_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_i2c *i2c = EXTRACT_I2C(env, obj);
	LOG_DEBUG("i2c is %p", i2c);

	RESULT_EXCEPTION_ACT(b0_i2c_close(i2c));
}
