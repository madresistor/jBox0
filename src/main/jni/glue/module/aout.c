/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_module_Aout.h"
#include "private.h"
#include <math.h>

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    propertyRef
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Aout/Ref;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Aout_propertyRef
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	return PROPERTY_REF(env, "Aout", aout->ref.low, aout->ref.high, aout->ref.type);
}

static jobjectArray mode_array(JNIEnv *env,
			const struct b0_aout_mode_bitsize_speeds *mode)
{
	size_t i;

	/* Get the class */
	const char *cls_name = "com/madresistor/box0/module/Aout$BitsizeSpeeds";
	jclass cls = (*env)->FindClass(env, cls_name);
	if (cls == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* Get the constructor of BitsizeSpeeds */
	jmethodID init = (*env)->GetMethodID(env, cls, "<init>",
				"(ILcom/madresistor/box0/extra/CUnsignedLongArray;)V");
	if ((*env)->ExceptionCheck(env)) {
		return JAVA_NULL;
	}

	/* Create an array of type BitsizeSpeeds */
	jobjectArray arr = (*env)->NewObjectArray(env, mode->count, cls, JAVA_NULL);
	if (arr == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* Populate the array */
	for (i = 0; i < mode->count; i++) {
		struct b0_aout_bitsize_speeds *bss = &mode->values[i];

		/* Build the contructor argument (speed) */
		jobject speeds = c_unsigned_long_array(env, bss->speed.values, bss->speed.count);
		if (speeds == JAVA_NULL) {
			return JAVA_NULL;
		}

		/* Call the BitsizeSpeeds contructor */
		jobject entry = (*env)->NewObject(env, cls, init, bss->bitsize, speeds);
		if (entry == JAVA_NULL) {
			return JAVA_NULL;
		}

		(*env)->DeleteLocalRef(env, speeds);

		/* Set the value in array */
		(*env)->SetObjectArrayElement(env, arr, i, entry);

		if ((*env)->ExceptionCheck(env)) {
			return JAVA_NULL;
		}

		(*env)->DeleteLocalRef(env, entry);
	}

	(*env)->DeleteLocalRef(env, cls);

	return arr;
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    propertyStream
 * Signature: (Ljava/nio/ByteBuffer;)[Lcom/madresistor/box0/module/Aout/BitsizeSpeeds;
 */
JNIEXPORT jobjectArray JNICALL Java_com_madresistor_box0_module_Aout_propertyStream
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	return mode_array(env, &aout->stream);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    propertySnapshot
 * Signature: (Ljava/nio/ByteBuffer;)[Lcom/madresistor/box0/module/Aout/BitsizeSpeeds;
 */
JNIEXPORT jobjectArray JNICALL Java_com_madresistor_box0_module_Aout_propertySnapshot
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	return mode_array(env, &aout->snapshot);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    propertyChanCount
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Aout_propertyChanCount
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	return aout->chan_count;
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    propertyBufferSize
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_com_madresistor_box0_module_Aout_propertyBufferSize
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	return aout->buffer_size;
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    propertyCapab
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Aout_propertyCapab
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	return aout->capab;
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    propertyLabel
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Aout/Label;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Aout_propertyLabel
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	jobjectArray arr = to_java_string_array(env, aout->label.chan,
				aout->chan_count);
	if (arr == JAVA_NULL) {
		return JAVA_NULL;
	}

	return PROPERTY_LABEL(env, "Aout", arr);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    streamPrepare
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_streamPrepare
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	RESULT_EXCEPTION_ACT(b0_aout_stream_prepare(aout));
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    streamWrite
 * Signature: (Ljava/nio/ByteBuffer;[D)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_streamWrite__Ljava_nio_ByteBuffer_2_3D
  (JNIEnv *env, jclass cls, jobject obj, jdoubleArray dataArr)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	jdouble *data = (*env)->GetDoubleArrayElements(env, dataArr, NULL);
	if (data == NULL) {
		return;
	}
	jsize len = (*env)->GetArrayLength(env, dataArr);

	b0_result_code r = b0_aout_stream_write_double(aout, data, (size_t)len);

	(*env)->ReleaseDoubleArrayElements(env, dataArr, data, JNI_ABORT);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    streamWrite
 * Signature: (Ljava/nio/ByteBuffer;[F)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_streamWrite__Ljava_nio_ByteBuffer_2_3F
  (JNIEnv *env, jclass cls, jobject obj, jfloatArray dataArr)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	jfloat *data = (*env)->GetFloatArrayElements(env, dataArr, NULL);
	if (data == NULL) {
		return;
	}
	jsize len = (*env)->GetArrayLength(env, dataArr);

	b0_result_code r = b0_aout_stream_write_float(aout, data, (size_t)len);

	(*env)->ReleaseFloatArrayElements(env, dataArr, data, JNI_ABORT);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    streamStart
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_streamStart
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	RESULT_EXCEPTION_ACT(b0_aout_stream_start(aout));
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    streamStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_streamStop
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	RESULT_EXCEPTION_ACT(b0_aout_stream_stop(aout));
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    streamStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_snapshotPrepare
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	RESULT_EXCEPTION_ACT(b0_aout_snapshot_prepare(aout));
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    snapshotStart
 * Signature: (Ljava/nio/ByteBuffer;[D)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_snapshotStart__Ljava_nio_ByteBuffer_2_3D
  (JNIEnv *env, jclass cls, jobject obj, jdoubleArray dataArr)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	jdouble *data = (*env)->GetDoubleArrayElements(env, dataArr, NULL);
	if (data == NULL) {
		return;
	}
	jsize len = (*env)->GetArrayLength(env, dataArr);

	b0_result_code r = b0_aout_snapshot_start_double(aout, data, (size_t)len);

	(*env)->ReleaseDoubleArrayElements(env, dataArr, data, JNI_ABORT);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    snapshotStart
 * Signature: (Ljava/nio/ByteBuffer;[F)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_snapshotStart__Ljava_nio_ByteBuffer_2_3F
  (JNIEnv *env, jclass cls, jobject obj, jfloatArray dataArr)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	jfloat *data = (*env)->GetFloatArrayElements(env, dataArr, NULL);
	if (data == NULL) {
		return;
	}
	jsize len = (*env)->GetArrayLength(env, dataArr);

	b0_result_code r = b0_aout_snapshot_start_float(aout, data, (size_t)len);

	(*env)->ReleaseFloatArrayElements(env, dataArr, data, JNI_ABORT);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    snapshotStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_snapshotStop
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	RESULT_EXCEPTION_ACT(b0_aout_snapshot_stop(aout));
}

/* Construct a Aout.SnapshotCalc */
static jobject snapshot_calc_object(JNIEnv *env, size_t count, unsigned long speed)
{
	static const char *clsName = "com/madresistor/box0/module/Aout$SnapshotCalc";
	jclass exClass = (*env)->FindClass (env, clsName);
	jmethodID constructor = (*env)->GetMethodID (env, exClass, "<init>", "(JJ)V");
	jlong arg0 = (jlong) count;
	jlong arg1 = (jlong) speed;
	return (*env)->NewObject(env, exClass, constructor, arg0, arg1);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    snapshotCalc
 * Signature: (Ljava/nio/ByteBuffer;DI)Lcom/madresistor/box0/module/Aout/SnapshotCalc;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Aout_snapshotCalc
  (JNIEnv *env, jclass cls, jobject obj, jdouble freq, jint bitsize)
{
	UNUSED(cls);
	unsigned long speed;
	size_t count;

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	RESULT_EXCEPTION_ACT(b0_aout_snapshot_calc(aout, (double) freq,
		(unsigned int) bitsize, &count, &speed), JAVA_NULL);

	return snapshot_calc_object(env, count, speed);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Aout_open
  (JNIEnv *env, jclass cls, jobject _dev, jint index)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, _dev);
	LOG_DEBUG("dev is %p", dev);

	b0_aout *aout;
	RESULT_EXCEPTION_ACT(b0_aout_open(dev, &aout, index), 0);

	return (*env)->NewDirectByteBuffer(env, aout, 0);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	RESULT_EXCEPTION_ACT(b0_aout_close(aout));
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    setRepeat
 * Signature: (Ljava/nio/ByteBuffer;J)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_setRepeat
  (JNIEnv *env, jclass cls, jobject obj, jlong value)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	RESULT_EXCEPTION_ACT(b0_aout_repeat_set(aout, (unsigned long) value));
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    getRepeat
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_com_madresistor_box0_module_Aout_getRepeat
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	unsigned long value;
	RESULT_EXCEPTION_ACT(b0_aout_repeat_get(aout, &value), 0);
	return value;
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    setBitsizeSpeed
 * Signature: (Ljava/nio/ByteBuffer;IJ)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_setBitsizeSpeed
  (JNIEnv *env, jclass cls, jobject obj, jint bitsize, jlong speed)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	RESULT_EXCEPTION_ACT(b0_aout_bitsize_speed_set(aout,
		(unsigned int) bitsize, (unsigned long) speed));
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    getBitsizeSpeed
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Aout/BitsizeSpeed;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Aout_getBitsizeSpeed
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	unsigned int bitsize;
	unsigned long speed;
	RESULT_EXCEPTION_ACT(b0_aout_bitsize_speed_get(aout, &bitsize, &speed), 0);

	jclass cls_bs = (*env)->FindClass(env, "com/madresistor/box0/module/Aout$BitsizeSpeed");
	if (cls_bs == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* BitsizeSpeed(int, long) */
	jmethodID init = (*env)->GetMethodID(env, cls_bs, "<init>", "(IJ)V");
	if ((*env)->ExceptionCheck(env)) {
		return JAVA_NULL;
	}

	return (*env)->NewObject(env, cls, init, bitsize, speed);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    setChanSeq
 * Signature: (Ljava/nio/ByteBuffer;[II)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Aout_setChanSeq
  (JNIEnv *env, jclass cls, jobject obj, jintArray _data, jint len)
{
	b0_result_code r;

	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	jint *data = (*env)->GetIntArrayElements(env, _data, NULL);
	if (data == NULL) {
		return;
	}

	r = b0_aout_chan_seq_set(aout, (unsigned int *) data, len);

	(*env)->ReleaseIntArrayElements(env, _data, data, JNI_ABORT);

	RESULT_EXCEPTION_ACT(r);
}

/*
 * Class:     com_madresistor_box0_module_Aout
 * Method:    getChanSeq
 * Signature: (Ljava/nio/ByteBuffer;)[I
 */
JNIEXPORT jintArray JNICALL Java_com_madresistor_box0_module_Aout_getChanSeq
  (JNIEnv *env, jclass cls, jobject obj)
{
	b0_result_code r;

	UNUSED(cls);

	b0_aout *aout = EXTRACT_AOUT(env, obj);
	LOG_DEBUG("aout is %p", aout);

	/* Perform memory allocation */
	size_t alloc_len = aout->chan_count + 1;
	unsigned int *data = NULL, *old_data;

	/* Repeatable point if the full list could not be readed */
	perform:
	data = realloc(old_data = data, sizeof(*data) * alloc_len);
	if (data == NULL) {
		if (old_data != NULL) {
			free(old_data);
		}

		ThrowRuntimeException("realloc returned NULL");
		return JAVA_NULL;
	}

	/* Fetch */
	size_t len = alloc_len;
	r = b0_aout_chan_seq_get(aout, data, &len);
	if (B0_ERR_RC(r)) {
		free(data);
		RESULT_EXCEPTION_ACT(r, JAVA_NULL);
	} else if (len == alloc_len) {
		alloc_len *= 2;
		goto perform;
	}

	/* Create array */
	jintArray arr = (*env)->NewIntArray(env, (jsize) len);
	if (arr == JAVA_NULL) {
		goto done;
	}

	/* Copy to array */
	(*env)->SetIntArrayRegion(env, arr, 0, len, (jint *) data);

	done:
	/* Free the allocate memory */
	free(data);

	return arr;
}
