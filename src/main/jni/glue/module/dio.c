/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_module_Dio.h"
#include "private.h"

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    setDir
 * Signature: (Ljava/nio/ByteBuffer;IZ)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_setDir
  (JNIEnv *env, jclass cls, jobject obj, jint index, jboolean dir)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);


	if (index < 0 || ((unsigned int) index) > dio->pin_count) {
		ThrowIllegalArgumentException("index");
		return;
	}

	bool d = BOOL_JAVA_TO_C(dir);
	RESULT_EXCEPTION_ACT(b0_dio_dir_set(dio, (unsigned int) index, d));
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    setValue
 * Signature: (Ljava/nio/ByteBuffer;IZ)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_setValue
  (JNIEnv *env, jclass cls, jobject obj, jint index, jboolean value)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);


	if (index < 0 || ((unsigned int) index) > dio->pin_count) {
		ThrowIllegalArgumentException("index");
		return;
	}

	bool v = BOOL_JAVA_TO_C(value);
	RESULT_EXCEPTION_ACT(b0_dio_value_set(dio, (unsigned int) index, v));
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    getDir
 * Signature: (Ljava/nio/ByteBuffer;I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_madresistor_box0_module_Dio_getDir
  (JNIEnv *env, jclass cls, jobject obj, jint index)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	if (index < 0 || ((unsigned int) index) > dio->pin_count) {
		ThrowIllegalArgumentException("index");
		return 0;
	}

	bool d;
	RESULT_EXCEPTION_ACT(b0_dio_dir_get(dio, (unsigned int) index, &d), 0);

	return BOOL_C_TO_JAVA(d);
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    getValue
 * Signature: (Ljava/nio/ByteBuffer;I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_madresistor_box0_module_Dio_getValue
  (JNIEnv *env, jclass cls, jobject obj, jint index)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	if (index < 0 || ((unsigned int) index) > dio->pin_count) {
		ThrowIllegalArgumentException("index");
		return 0;
	}

	bool v;
	RESULT_EXCEPTION_ACT(b0_dio_value_get(dio, (unsigned int) index, &v), 0);

	return BOOL_C_TO_JAVA(v);
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    toggle
 * Signature: (Ljava/nio/ByteBuffer;I)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_toggle
  (JNIEnv *env, jclass cls, jobject obj, jint index)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	if (index < 0 || ((unsigned int) index) > dio->pin_count) {
		ThrowIllegalArgumentException("index");
		return;
	}

	RESULT_EXCEPTION_ACT(b0_dio_value_toggle(dio, (unsigned int) index));
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    propertyRef
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Dio/Ref;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Dio_propertyRef
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	return PROPERTY_REF(env, "Dio", dio->ref.low, dio->ref.high, dio->ref.type);
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    propertyPinCount
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Dio_propertyPinCount
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	return dio->pin_count;
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    propertyCapab
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Dio_propertyCapab
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	return dio->capab;
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    propertyLabel
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Dio/Label;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Dio_propertyLabel
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);
	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	jobjectArray arr = to_java_string_array(env, dio->label.pin,
				dio->pin_count);
	if (arr == JAVA_NULL) {
		return JAVA_NULL;
	}

	return PROPERTY_LABEL(env, "Dio", arr);
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Dio_open
  (JNIEnv *env, jclass cls, jobject _dev, jint index)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, _dev);
	LOG_DEBUG("dev is %p", dev);

	b0_dio *dio;
	RESULT_EXCEPTION_ACT(b0_dio_open(dev, &dio, index), 0);

	return (*env)->NewDirectByteBuffer(env, dio, 0);
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	RESULT_EXCEPTION_ACT(b0_dio_close(dio));
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    basicPrepare
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_basicPrepare
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	RESULT_EXCEPTION_ACT(b0_dio_basic_prepare(dio));
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    basicStart
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_basicStart
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	RESULT_EXCEPTION_ACT(b0_dio_basic_start(dio));
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    basicStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_basicStop
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	RESULT_EXCEPTION_ACT(b0_dio_basic_stop(dio));
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    setHiz
 * Signature: (Ljava/nio/ByteBuffer;IZ)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_setHiz
  (JNIEnv *env, jclass cls, jobject obj, jint index, jboolean value)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);


	if (index < 0 || ((unsigned int) index) > dio->pin_count) {
		ThrowIllegalArgumentException("index");
		return;
	}

	bool v = BOOL_JAVA_TO_C(value);
	RESULT_EXCEPTION_ACT(b0_dio_hiz_set(dio, (unsigned int) index, v));
}

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    getHiz
 * Signature: (Ljava/nio/ByteBuffer;I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_madresistor_box0_module_Dio_getHiz
  (JNIEnv *env, jclass cls, jobject obj, jint index)
{
	UNUSED(cls);

	b0_dio *dio = EXTRACT_DIO(env, obj);
	LOG_DEBUG("dio is %p", dio);

	if (index < 0 || ((unsigned int) index) > dio->pin_count) {
		ThrowIllegalArgumentException("index");
		return 0;
	}

	bool v;
	RESULT_EXCEPTION_ACT(b0_dio_hiz_get(dio, (unsigned int) index, &v), 0);

	return BOOL_C_TO_JAVA(v);
}
