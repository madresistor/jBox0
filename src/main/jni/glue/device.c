/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_Device.h"
#include "private.h"
#include <string.h>
#include <stdio.h>

/*
 * Class:     com_madresistor_box0_Device
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_Device_close
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_device *device = EXTRACT_DEVICE(env, obj);
	LOG_DEBUG("device is %p", device);

	RESULT_EXCEPTION_ACT(b0_device_close(device));
}

/*
 * Class:     com_madresistor_box0_Device
 * Method:    log
 * Signature: (Ljava/nio/ByteBuffer;I)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_Device_log
  (JNIEnv *env, jclass cls, jobject obj, jint log_level)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, obj);
	LOG_DEBUG("device is %p", dev);

	RESULT_EXCEPTION_ACT(b0_device_log(dev, log_level));
}

/*
 * Class:     com_madresistor_box0_Device
 * Method:    getManuf
 * Signature: (Ljava/nio/ByteBuffer;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_madresistor_box0_Device_getManuf
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, obj);
	LOG_DEBUG("device is %p", dev);

	return (*env)->NewStringUTF(env, (char const *)dev->manuf);
}

/*
 * Class:     com_madresistor_box0_Device
 * Method:    getName
 * Signature: (Ljava/nio/ByteBuffer;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_madresistor_box0_Device_getName
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, obj);
	LOG_DEBUG("device is %p", dev);

	return (*env)->NewStringUTF(env, (char const *)dev->name);
}

/*
 * Class:     com_madresistor_box0_Device
 * Method:    getSerial
 * Signature: (Ljava/nio/ByteBuffer;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_madresistor_box0_Device_getSerial
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, obj);
	LOG_DEBUG("device is %p", dev);

	return (*env)->NewStringUTF(env, (char const *)dev->serial);
}

/*
 * Class:     com_madresistor_box0_Device
 * Method:    ping
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_Device_ping
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	b0_device *dev = EXTRACT_DEVICE(env, obj);
	LOG_DEBUG("device is %p", dev);

	RESULT_EXCEPTION_ACT(b0_device_ping(dev));
}
