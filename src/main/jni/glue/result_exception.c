/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "com_madresistor_box0_ResultException.h"
#include "private.h"

/*
 * Class:     com_madresistor_box0_ResultException
 * Method:    name
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_madresistor_box0_ResultException_name
  (JNIEnv *env, jclass cls, jint err)
  {
	UNUSED(cls);

	const uint8_t *name = b0_result_name((int)err);
	return (*env)->NewStringUTF(env, (const char *) name);
}

/*
 * Class:     com_madresistor_box0_ResultException
 * Method:    explain
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_madresistor_box0_ResultException_explain
  (JNIEnv *env, jclass cls, jint err)
{
	UNUSED(cls);

	const uint8_t *desc = b0_result_explain((int)err);
	return (*env)->NewStringUTF(env, (const char *) desc);
}
