/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "private.h"
#include "com_madresistor_box0_backend_Usb.h"

#define EXTRACT_USBC(env, obj)	EXTRACT_POINTER(env, obj, libusb_context)
#define EXTRACT_USBD(env, obj)	EXTRACT_POINTER(env, obj, libusb_device)
#define EXTRACT_USBDH(env, obj)	EXTRACT_POINTER(env, obj, libusb_device_handle)

/*
 * Class:     com_madresistor_box0_backend_Usb
 * Method:    openVidPid
 * Signature: (II)Lcom/madresistor/box0/Device;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_backend_Usb_openVidPid
  (JNIEnv *env, jclass cls, jint vid, jint pid)
{
	UNUSED(cls);

	b0_device *dev;
	RESULT_EXCEPTION_ACT(b0_usb_open_vid_pid(&dev, (uint16_t)vid, (uint16_t)pid), 0);
	return NewDevice(dev);
}

/*
 * Class:     com_madresistor_box0_backend_Usb
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/Device;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_backend_Usb_open
  (JNIEnv *env, jclass cls, jobject _usbd, jobject _usbc)
{
	UNUSED(cls);

	libusb_device *usbd = EXTRACT_USBD(env, _usbd);
	if (usbd == NULL) {
		ThrowIllegalArgumentException("Device pointer is NULL");
		return 0;
	}

	libusb_context *usbc = EXTRACT_USBC(env, _usbc);
	if (usbc == NULL) {
		ThrowIllegalArgumentException("Context pointer is NULL");
		return 0;
	}

	b0_device *dev;
	RESULT_EXCEPTION_ACT(b0_usb_open(usbd, usbc, &dev), 0);
	return NewDevice(dev);
}

/*
 * Class:     com_madresistor_box0_backend_Usb
 * Method:    openHandle
 * Signature: (Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/Device;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_backend_Usb_openHandle
  (JNIEnv *env, jclass cls, jobject _usbdh, jobject _usbc)
{
	UNUSED(cls);

	libusb_device_handle *usbdh = EXTRACT_USBDH(env, _usbdh);
	if (usbdh == NULL) {
		ThrowIllegalArgumentException("Device Handle pointer is NULL");
		return 0;
	}

	libusb_context *usbc = EXTRACT_USBC(env, _usbc);
	if (usbc == NULL) {
		ThrowIllegalArgumentException("Context pointer is NULL");
		return 0;
	}

	b0_device *dev;
	RESULT_EXCEPTION_ACT(b0_usb_open_handle(usbdh, usbc, &dev), 0);
	return NewDevice(dev);
}

/*
 * Class:     com_madresistor_box0_backend_Usb
 * Method:    openSupported
 * Signature: ()Lcom/madresistor/box0/Device;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_backend_Usb_openSupported
  (JNIEnv *env, jclass cls)
{
	UNUSED(cls);

	b0_device *dev;
	RESULT_EXCEPTION_ACT(b0_usb_open_supported(&dev), 0);
	return NewDevice(dev);
}
