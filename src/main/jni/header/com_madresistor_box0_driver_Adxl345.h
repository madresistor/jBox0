/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_madresistor_box0_driver_Adxl345 */

#ifndef _Included_com_madresistor_box0_driver_Adxl345
#define _Included_com_madresistor_box0_driver_Adxl345
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_madresistor_box0_driver_Adxl345
 * Method:    openI2c
 * Signature: (Ljava/nio/ByteBuffer;Z)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_driver_Adxl345_openI2c
  (JNIEnv *, jclass, jobject, jboolean);

/*
 * Class:     com_madresistor_box0_driver_Adxl345
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Adxl345_close
  (JNIEnv *, jclass, jobject);

/*
 * Class:     com_madresistor_box0_driver_Adxl345
 * Method:    powerUp
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Adxl345_powerUp
  (JNIEnv *, jclass, jobject);

/*
 * Class:     com_madresistor_box0_driver_Adxl345
 * Method:    read
 * Signature: (Ljava/nio/ByteBuffer;[D)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_driver_Adxl345_read
  (JNIEnv *, jclass, jobject, jdoubleArray);

#ifdef __cplusplus
}
#endif
#endif
