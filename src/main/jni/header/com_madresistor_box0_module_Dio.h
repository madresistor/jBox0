/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_madresistor_box0_module_Dio */

#ifndef _Included_com_madresistor_box0_module_Dio
#define _Included_com_madresistor_box0_module_Dio
#ifdef __cplusplus
extern "C" {
#endif
#undef com_madresistor_box0_module_Dio_DIO
#define com_madresistor_box0_module_Dio_DIO 1L
#undef com_madresistor_box0_module_Dio_AOUT
#define com_madresistor_box0_module_Dio_AOUT 2L
#undef com_madresistor_box0_module_Dio_AIN
#define com_madresistor_box0_module_Dio_AIN 3L
#undef com_madresistor_box0_module_Dio_SPI
#define com_madresistor_box0_module_Dio_SPI 4L
#undef com_madresistor_box0_module_Dio_I2C
#define com_madresistor_box0_module_Dio_I2C 5L
#undef com_madresistor_box0_module_Dio_PWM
#define com_madresistor_box0_module_Dio_PWM 6L
#undef com_madresistor_box0_module_Dio_UNIO
#define com_madresistor_box0_module_Dio_UNIO 7L
#undef com_madresistor_box0_module_Dio_INPUT
#define com_madresistor_box0_module_Dio_INPUT 0L
#undef com_madresistor_box0_module_Dio_OUTPUT
#define com_madresistor_box0_module_Dio_OUTPUT 1L
#undef com_madresistor_box0_module_Dio_LOW
#define com_madresistor_box0_module_Dio_LOW 0L
#undef com_madresistor_box0_module_Dio_HIGH
#define com_madresistor_box0_module_Dio_HIGH 1L
#undef com_madresistor_box0_module_Dio_DISABLE
#define com_madresistor_box0_module_Dio_DISABLE 0L
#undef com_madresistor_box0_module_Dio_ENABLE
#define com_madresistor_box0_module_Dio_ENABLE 1L
/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    setDir
 * Signature: (Ljava/nio/ByteBuffer;IZ)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_setDir
  (JNIEnv *, jclass, jobject, jint, jboolean);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    setValue
 * Signature: (Ljava/nio/ByteBuffer;IZ)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_setValue
  (JNIEnv *, jclass, jobject, jint, jboolean);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    getDir
 * Signature: (Ljava/nio/ByteBuffer;I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_madresistor_box0_module_Dio_getDir
  (JNIEnv *, jclass, jobject, jint);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    getValue
 * Signature: (Ljava/nio/ByteBuffer;I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_madresistor_box0_module_Dio_getValue
  (JNIEnv *, jclass, jobject, jint);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    toggle
 * Signature: (Ljava/nio/ByteBuffer;I)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_toggle
  (JNIEnv *, jclass, jobject, jint);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    setHiz
 * Signature: (Ljava/nio/ByteBuffer;IZ)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_setHiz
  (JNIEnv *, jclass, jobject, jint, jboolean);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    getHiz
 * Signature: (Ljava/nio/ByteBuffer;I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_madresistor_box0_module_Dio_getHiz
  (JNIEnv *, jclass, jobject, jint);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    propertyRef
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Dio/Ref;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Dio_propertyRef
  (JNIEnv *, jclass, jobject);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    propertyPinCount
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Dio_propertyPinCount
  (JNIEnv *, jclass, jobject);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    propertyCapab
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_madresistor_box0_module_Dio_propertyCapab
  (JNIEnv *, jclass, jobject);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    propertyLabel
 * Signature: (Ljava/nio/ByteBuffer;)Lcom/madresistor/box0/module/Dio/Label;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Dio_propertyLabel
  (JNIEnv *, jclass, jobject);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    open
 * Signature: (Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_com_madresistor_box0_module_Dio_open
  (JNIEnv *, jclass, jobject, jint);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_close
  (JNIEnv *, jclass, jobject);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    basicPrepare
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_basicPrepare
  (JNIEnv *, jclass, jobject);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    basicStart
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_basicStart
  (JNIEnv *, jclass, jobject);

/*
 * Class:     com_madresistor_box0_module_Dio
 * Method:    basicStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_com_madresistor_box0_module_Dio_basicStop
  (JNIEnv *, jclass, jobject);

#ifdef __cplusplus
}
#endif
#endif
