/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JBOX0_COMMON_H
#define JBOX0_COMMON_H

/* __BEGIN_DECLS should be used at the beginning of your declarations,
   so that C++ compilers don't mangle their names.  Use __END_DECLS at
   the end of C declarations. */
#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

/* Reference: http://gcc.gnu.org/wiki/Visibility */
#if __GNUC__ >= 4
	#define JBOX0_API __attribute__ ((visibility ("default")))
	#define JBOX0_PRIV  __attribute__ ((visibility ("hidden")))
#else /* __GNUC__ >= 4 */
	#define JBOX0_API
	#define JBOX0_PRIV
#endif /* __GNUC__ >= 4 */

__END_DECLS

#endif
