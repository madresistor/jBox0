/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JBOX0_PRIVATE_H
#define JBOX0_PRIVATE_H

#include <jni.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <stdlib.h>
#include "jbox0.h"

__BEGIN_DECLS

#define BOOL_C_TO_JAVA(value) ((value) ? JNI_TRUE : JNI_FALSE)
#define BOOL_JAVA_TO_C(value) ((value) != JNI_FALSE)

#undef UNUSED
#define UNUSED(var) ((void)var)

#define _LOG_TAG "jbox0"

#undef NDEBUG
#if !defined(NDEBUG)
# if defined(__ANDROID__)
#  include <android/log.h>
#  define LOG_DEBUG(fmt, ...) __android_log_print(ANDROID_LOG_DEBUG,	\
	_LOG_TAG, "%s:%i: " fmt, __func__, __LINE__, ##__VA_ARGS__)
# else /* defined(__ANDROID__) */
#  include <stdio.h>
#  define LOG_DEBUG(fmt, ...)											\
	fprintf(stderr, _LOG_TAG ": %s:%i: " fmt "\n",						\
		__func__, __LINE__, ##__VA_ARGS__)
# endif /* defined(__ANDROID__) */
#else /* !defined(NDEBUG) */
# define LOG_DEBUG(msg, ...)
#endif /* !defined(NDEBUG) */

#define RESULT_EXCEPTION_ACT(stmt, ...) {								\
	b0_result_code _r = (stmt);											\
	if (B0_ERROR_RESULT_CODE(_r)) {										\
		ThrowResultException(_r);										\
		return __VA_ARGS__;												\
	}																	\
}

#define ThrowResultException(r)											\
	LOG_DEBUG("throw new ResultException(%i) [%s:%s]",					\
		r, b0_result_name(r), b0_result_explain(r));					\
	jbox0_throw_result_exception(env, r);

#define ThrowRuntimeException(msg)										\
	LOG_DEBUG("throw new RuntimeException(\"%s\")", msg);				\
	throw_exception(env, "java/lang/RuntimeException", msg);

#define ThrowIndexOutOfBoundsException(msg)								\
	LOG_DEBUG("throw new IndexOutOfBoundsException(\"%s\")", msg);		\
	throw_exception(env, "java/lang/IndexOutOfBoundsException", msg);

#define ThrowIllegalArgumentException(msg)								\
	LOG_DEBUG("throw new IllegalArgumentException(\"%s\")", msg);		\
	throw_exception(env, "java/lang/IllegalArgumentException", msg);

#define MallocCheck(ptr, ...)											\
	if (ptr == NULL) {													\
		ThrowRuntimeException("malloc returned NULL");					\
		return __VA_ARGS__;												\
	}																	\

#define ReallocCheck(ptr, ...)											\
	if (ptr == NULL) {													\
		ThrowRuntimeException("realloc returned NULL");					\
		return __VA_ARGS__;												\
	}																	\

#define JAVA_NULL 0

#define ENSURE_GREATER_THAN_ZERO(env, value, ...) {				\
	if (value <= 0) {											\
		ThrowRuntimeException(#value " is not greater than 0");	\
		return __VA_ARGS__;										\
	}															\
}

#define ENSURE_ATLEAST_LENGTH_OF_ARRAY(env, data, atleast, ...) {	\
	jint _length = (*env)->GetArrayLength(env, data);				\
	if ((*env)->ExceptionCheck(env)) {								\
		return __VA_ARGS__;											\
	}																\
																	\
	if (atleast > _length) {										\
		ThrowRuntimeException(#data " length is smaller"				\
			" than " #atleast);										\
		return __VA_ARGS__;											\
	}																\
}

void throw_exception(JNIEnv *env, const char *cls, const char *msg);

#define NewDevice(dev) jbox0_device(env, dev)

#define EXTRACT_POINTER(env, obj, type)	((type *)(*env)->GetDirectBufferAddress(env, obj))
#define EXTRACT_DEVICE(env, obj)		EXTRACT_POINTER(env, obj, b0_device)
#define EXTRACT_MODULE(env, obj)		EXTRACT_POINTER(env, obj, b0_module)
#define EXTRACT_AIN(env, obj)			EXTRACT_POINTER(env, obj, b0_ain)
#define EXTRACT_AOUT(env, obj)			EXTRACT_POINTER(env, obj, b0_aout)
#define EXTRACT_DIO(env, obj)			EXTRACT_POINTER(env, obj, b0_dio)
#define EXTRACT_I2C(env, obj)			EXTRACT_POINTER(env, obj, b0_i2c)
#define EXTRACT_PWM(env, obj)			EXTRACT_POINTER(env, obj, b0_pwm)
#define EXTRACT_SPI(env, obj)			EXTRACT_POINTER(env, obj, b0_spi)
#define EXTRACT_ADXL345(env, obj)		EXTRACT_POINTER(env, obj, b0_adxl345)
#define EXTRACT_BMP180(env, obj)		EXTRACT_POINTER(env, obj, b0_bmp180)
#define EXTRACT_L3GD20(env, obj)		EXTRACT_POINTER(env, obj, b0_l3gd20)
#define EXTRACT_LM75(env, obj)			EXTRACT_POINTER(env, obj, b0_lm75)
#define EXTRACT_MAX31855(env, obj)		EXTRACT_POINTER(env, obj, b0_max31855)
#define EXTRACT_SI1145(env, obj)		EXTRACT_POINTER(env, obj, b0_si114x)
#define EXTRACT_TMP006(env, obj)		EXTRACT_POINTER(env, obj, b0_tmp006)
#define EXTRACT_TSL2591(env, obj)		EXTRACT_POINTER(env, obj, b0_tsl2591)

bool spi_cache_sugar_arg_field(JNIEnv *env);
bool spi_cache_task_field(JNIEnv *env);
bool i2c_cache_task_field(JNIEnv *env);
bool i2c_cache_sugar_arg_field(JNIEnv *env);

#define PROPERTY_REF(env, cls_name, low, high, type)				\
	module_ref_with_type(env,										\
		"com/madresistor/box0/module/"  cls_name  "$Ref",			\
		low, high, type)

#define PROPERTY_LABEL(env, cls_name, arr)							\
	module_label_with_string_array(env,								\
		"com/madresistor/box0/module/"  cls_name "$Label", arr)

jobject module_ref_with_type(JNIEnv *env, const char *clz_name,
					double low, double high, int type);

jobjectArray module_label_with_string_array(JNIEnv *env,
		const char *clz_name, jobjectArray);

jobjectArray to_java_string_array(JNIEnv *env, uint8_t **values, size_t count);

jobject c_unsigned_int_array(JNIEnv *env, unsigned int *values, size_t count);
jobject c_unsigned_long_array(JNIEnv *env, unsigned long *values, size_t count);

__END_DECLS

#endif
