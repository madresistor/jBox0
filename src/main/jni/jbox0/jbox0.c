/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "private.h"

/**
 * construct a jBox0 device from libbox0 device
 * @param env JNI Env
 * @param dev libbox0 device
 * @return jBox0 device
 */
jobject jbox0_device(JNIEnv *env, b0_device *dev)
{
	const char *cls = "com/madresistor/box0/Device";
	const char *meth = "<init>";
	const char *sig = "(Ljava/nio/ByteBuffer;)V";

	jobject dev_bb = (*env)->NewDirectByteBuffer(env, dev, 0);
	jclass exClass = (*env)->FindClass(env, cls);
	jmethodID constructor =  (*env)->GetMethodID(env, exClass, meth, sig);
	return (*env)->NewObject(env, exClass, constructor, dev_bb);
}

/**
 * Construct a jBox0 ResultException
 * @details  construct a jBox0 ResultException throwable from libbox0 result code
 * @param env JNI Env
 * @param r libbox0 result code
 * @return ResultException throwable object
 */
jthrowable jbox0_result_exception(JNIEnv *env, b0_result_code r)
{
	const char *cls = "com/madresistor/box0/ResultException";
	const char *meth = "<init>";
	const char *sig = "(I)V";

	jclass exClass =  (*env)->FindClass(env, cls);
	jmethodID constructor = (*env)->GetMethodID(env, exClass, meth, sig);
	jobject exception = (*env)->NewObject(env, exClass, constructor, (jint) r);
	(*env)->DeleteLocalRef(env, exClass);

	return (jthrowable) exception;
}

/**
 * throw a jBox0 result exception
 * @details  throw a jBox0 ResultException from libbox0 result code
 * @param env JNI Env
 * @param r libbox0 result code
 */
void jbox0_throw_result_exception(JNIEnv *env, b0_result_code r)
{
	jthrowable exception = jbox0_result_exception(env, r);
	(*env)->Throw(env, exception);
}
