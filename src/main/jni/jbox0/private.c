/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "private.h"

/* ref: http://planet.jboss.org/post/pointers_in_jni_c */
/* ref: http://monochrome.sutic.nu/2013/09/01/nice-jni-exceptions.html */

void throw_exception(JNIEnv *env, const char *cls, const char *msg)
{
	jstring sMsg = (*env)->NewStringUTF(env, msg);
	jclass exClass =  (*env)->FindClass (env, cls);
	jmethodID constructor =  (*env)->GetMethodID (env, exClass, "<init>",
											"(Ljava/lang/String;)V");
	jobject exception = (*env)->NewObject(env, exClass, constructor, sMsg);
	(*env)->DeleteLocalRef (env, exClass);
	(*env)->Throw(env, (jthrowable) exception);
}

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
	UNUSED(reserved);

	JNIEnv* env;

	if ((*vm)->GetEnv(vm, (void **) &env, JNI_VERSION_1_6) != JNI_OK) {
		return JNI_ERR;
	}

	if (!spi_cache_sugar_arg_field(env) ||
		!spi_cache_task_field(env) ||
		!i2c_cache_sugar_arg_field(env) ||
		!i2c_cache_task_field(env)) {
		return JNI_ERR;
	}

	return JNI_VERSION_1_6;
}

jobject module_ref_with_type(JNIEnv *env, const char *cls_name,
					double low, double high, int type)
{
	jclass cls = (*env)->FindClass(env, cls_name);
	if (cls == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* Ref(double, double, int) */
	jmethodID init = (*env)->GetMethodID(env, cls, "<init>", "(DDI)V");
	if (init == JAVA_NULL) {
		return JAVA_NULL;
	}

	jobject res = (*env)->NewObject(env, cls, init, low, high, type);

	(*env)->DeleteLocalRef(env, cls);

	return res;
}

jobjectArray module_label_with_string_array(JNIEnv *env,
		const char *cls_name, jobjectArray arr)
{
	jclass cls = (*env)->FindClass(env, cls_name);
	if (cls == JAVA_NULL) {
		return JAVA_NULL;
	}

	/* Ref(double, double, int) */
	jmethodID init = (*env)->GetMethodID(env, cls, "<init>", "([Ljava/lang/String;)V");
	if (init == JAVA_NULL) {
		return JAVA_NULL;
	}

	jobjectArray res = (*env)->NewObject(env, cls, init, arr);

	(*env)->DeleteLocalRef(env, cls);

	return res;
}

jobjectArray to_java_string_array(JNIEnv *env, uint8_t **values, size_t count)
{
	size_t i;

	const char *cls_name = "java/lang/String";
	jclass cls =  (*env)->FindClass (env, cls_name);

	jobjectArray arr = (*env)->NewObjectArray(env, count, cls, JAVA_NULL);
	if (arr == JAVA_NULL) {
		return JAVA_NULL;
	}

	(*env)->DeleteLocalRef(env, cls);

	for (i = 0; i < count; i++) {
		const uint8_t *u8 = values[i];

		if (u8 == NULL) {
			/* Keep NULL */
			continue;
		}

		jstring str = (*env)->NewStringUTF(env, (const char *) u8);
		if (str == JAVA_NULL) {
			return JAVA_NULL;
		}

		(*env)->SetObjectArrayElement(env, arr, i, str);
		if ((*env)->ExceptionCheck(env)) {
			return JAVA_NULL;
		}

		(*env)->DeleteLocalRef(env, str);
	}

	return arr;
}
