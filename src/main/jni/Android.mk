#
# This file is part of jBox0.
#
# Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# jBox0 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# jBox0 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
#

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

define all-c-files-under
	$(patsubst ./%, %, 													\
		$(shell cd $(LOCAL_PATH) ; 										\
			find $(1) -name "*.c" -and -not -name ".*")					 \
		)
endef

LOCAL_SRC_FILES:= $(call all-c-files-under, .)

LOCAL_C_INCLUDES:= 														\
	$(LOCAL_PATH)														\
	$(LOCAL_PATH)/jbox0													\
	$(LOCAL_PATH)/header
LOCAL_EXPORT_C_INCLUDES:= $(LOCAL_PATH)

APP_ABI:= all
LOCAL_LDLIBS:= -llog
LOCAL_SHARED_LIBRARIES:= box0 usb-1.0
LOCAL_MODULE:= jbox0

ifeq ($(NDK_DEBUG),"1")
# debug build
LOCAL_CFLAGS += -g -Wall -Wwrite-strings -Wsign-compare
LOCAL_CFLAGS += -Wextra  -Wno-empty-body -Wno-unused-parameter
LOCAL_CFLAGS += -Wformat-security
endif

include $(BUILD_SHARED_LIBRARY)

# provide LIBBOX0_PATH=<libbox0-source-code-dir> LIBUSB_PATH=<libusb-source-code-dir>

#IMPORTANT NOTE:
# in libusb   in libusb.mk replace "libusb1.0" with "usb-1.0"
#  dont know why those guys did that :|
# also comment `include $(LOCAL_PATH)/examples.mk` and `include $(LOCAL_PATH)/tests.mk` in Android.mk
# if you need enable debug logging facility, try #define ENABLE_DEBUG_LOGGING   - see config.h
import-it = 										\
	$(call import-add-path,$(shell dirname $(1)));	\
	$(call import-module,$(shell basename $(1))/$(2))

$(call import-it,$(LIBUSB_PATH),android/jni)
$(call import-it,$(LIBBOX0_PATH),android/jni)
