/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.module;

import java.nio.ByteBuffer;

import com.madresistor.box0.module.ModuleInstance;
import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.extra.CUnsignedIntArray;
import com.madresistor.box0.extra.CUnsignedLongArray;

public class Spi extends ModuleInstance {
	public static class Label {
		public final String sclk, mosi, miso;
		public final String[] ss;
		public Label(String sclk, String mosi, String miso, String[] ss) {
			this.sclk = sclk;
			this.mosi = mosi;
			this.miso = miso;
			this.ss = ss;
		}
	}

	public static class Ref {
		public final double low, high;
		public final int type;
		public Ref(double low, double high, int type) {
			this.low = low;
			this.high = high;
			this.type = type;
		}
	}

	public final int ssCount;
	public final Label label;
	public final CUnsignedIntArray bitsize;
	public final CUnsignedLongArray speed;
	public final Ref ref;

	public Spi(Device device, int index) throws ResultException {
		super(open(device.getPointer(), index), device);

		ByteBuffer module = getPointer();
		ssCount = propertySsCount(module);
		label = propertyLabel(module);
		bitsize = propertyBitsize(module);
		speed = propertySpeed(module);
		ref = propertyRef(module);
	}

	public void masterPrepare() throws ResultException {
		masterPrepare(getPointer());
	}

	/* TODO: like Dio.Pin class, make Slave class */

	public static class Task {
		public static final int
			TASK_LAST = (1 << 0), /* Last task to execute */

			TASK_CPHA = (1 << 1),
			TASK_CPOL = (1 << 2),

			TASK_MODE0 = 0,
			TASK_MODE1 = TASK_CPHA,
			TASK_MODE2 = TASK_CPOL,
			TASK_MODE3 = TASK_CPOL | TASK_CPHA,

			TASK_FD = (0x0 << 3),
			TASK_HD_READ = (0x3 << 3),
			TASK_HD_WRITE = (0x1 << 3),

			TASK_MSB_FIRST = (0 << 5),
			TASK_LSB_FIRST = (1 << 5),

			TASK_MODE_MASK = (0x3 << 1),
			TASK_DUPLEX_MASK = (0x3 << 3),
			TASK_ENDIAN_MASK = (1 << 5);

		public final int flags; /**< Task flags */
		public final int addr;  /**< Slave address */
		public final int bitsize; /**< Bitsize to use for transfer */
		public final long speed; /** Speed to use in transfer (0 for fallback) */
		public final byte[] wdata; /**< Write memory */
		public final byte[] rdata; /**< Read memory */
		public final int count; /**< Number of data unit */

		public Task(int flags, int addr, int bitsize, long speed, byte[] wdata,
				byte[] rdata, int count) {
			this.flags = flags;
			this.addr = addr;
			this.bitsize = bitsize;
			this.speed = speed;
			this.wdata = wdata;
			this.rdata = rdata;
			this.count = count;
		}
	}

	public static class TaskFailed {
		public final int index;
		public final int count;
		public TaskFailed(int index, int count) {
			this.index = index;
			this.count = count;
		}
	}

	private final static TaskFailed INVALID_FAILED_TASK =
		new TaskFailed(-1, -1);

	public TaskFailed masterStart(Task task) throws ResultException {
		return masterStart(new Task[] { task });
	}

	public TaskFailed masterStart(Task[] tasks) throws ResultException {
		int[] task_failed = new int[2];
		masterStart(getPointer(), tasks, task_failed);

		if (task_failed[0] >= 0 || task_failed[1] >= 0) {
			return new TaskFailed(task_failed[0], task_failed[1]);
		} else {
			return INVALID_FAILED_TASK;
		}
	}

	public void masterStop() throws ResultException {
		masterStop(getPointer());
	}

	public static class SugarArg {
		public final int addr;
		public final int flags; /**< Task flags */
		public final long speed;
		public final int bitsize;

		public SugarArg(int addr, int flags, long speed, int bitsize) {
			this.addr = addr;
			this.flags = flags;
			this.speed = speed;
			this.bitsize = bitsize;
		}

		public SugarArg(int addr, int flags, int bitsize) {
			this(addr, flags, 0, bitsize);
		}
	}

	public byte[] masterFd(SugarArg arg, byte[] write, byte[] read,
			int count) throws ResultException {
		masterFd(getPointer(), arg, write, read, count);
		return read;
	}

	public byte[] masterFd(SugarArg arg, byte[] write, byte[] read)
			throws ResultException {
		return masterFd(arg, write, read, Math.min(write.length, read.length));
	}

	public byte[] masterFd(SugarArg arg, byte[] write)
			throws ResultException {
		return masterFd(arg, write, new byte[write.length], write.length);
	}

	public void masterHdWrite(SugarArg arg, byte[] data, int count)
			throws ResultException {
		masterHdWrite(getPointer(), arg, data, count);
	}

	public void masterHdWrite(SugarArg arg, byte[] data)
			throws ResultException {
		masterHdWrite(arg, data, data.length);
	}

	public byte[] masterHdRead(SugarArg arg, byte[] data, int count)
			throws ResultException {
		masterHdRead(getPointer(), arg, data, count);
		return data;
	}

	public byte[] masterHdRead(SugarArg arg, byte[] data)
			throws ResultException {
		return masterHdRead(arg, data, data.length);
	}

	public byte[] masterHdRead(SugarArg arg, int count)
			throws ResultException {
		return masterHdRead(arg, new byte[count], count);
	}

	public byte[] masterHdWriteRead(SugarArg arg, byte[] write_data, int write_count,
			byte[] read_data, int read_count) throws ResultException {
		masterHdWriteRead(getPointer(), arg, write_data, write_count, read_data,
			read_count);
		return read_data;
	}

	public byte[] masterHdWriteRead(SugarArg arg, byte[] write, byte[] read)
			throws ResultException {
		return masterHdWriteRead(arg, write, write.length, read, read.length);
	}

	public byte[] masterHdWriteRead(SugarArg arg, byte[] write, int read)
			throws ResultException {
		return masterHdWriteRead(arg, write, write.length, new byte[read], read);
	}

	public void setActiveState(int addr, boolean value)
			throws ResultException {
		setActiveState(getPointer(), addr, value);
	}

	public boolean getActiveState(int addr)
			throws ResultException {
		return getActiveState(getPointer(), addr);
	}

	@Override
	public void close() throws ResultException {
		close(getPointer());
		unrefPointer();
	}

	private native static int propertySsCount(
		ByteBuffer /* (b0_spi*) */ mod);

	private native static Ref propertyRef(
		ByteBuffer /* (b0_spi*) */ mod);

	private native static CUnsignedIntArray propertyBitsize(
		ByteBuffer /* (b0_spi*) */ mod);

	private native static CUnsignedLongArray propertySpeed(
		ByteBuffer /* (b0_spi*) */ mod);

	private native static Label propertyLabel(
		ByteBuffer /* (b0_spi*) */ mod);

	private native static ByteBuffer /* (b0_ain*) */ open(
		ByteBuffer /* (b0_device*) */ dev, int index)
			throws ResultException;

	private native static void close(ByteBuffer /* (b0_ain*) */ mod)
			throws ResultException;

	private native static void masterPrepare(ByteBuffer /* (b0_spi*) */ mod)
		throws ResultException;

	private native static void masterStart(ByteBuffer /* (b0_spi*) */ mod,
		Task[] tasks, int[] failed_task /* [0] = Index, [1] = Count */)
		throws ResultException;

	private native static void masterStop(ByteBuffer /* (b0_spi*) */ mod)
		throws ResultException;

	private native static void masterFd(ByteBuffer /* (b0_spi*) */ mod,
		SugarArg arg, byte[] write, byte[] read, int count)
			throws ResultException;

	private native static void masterHdRead(ByteBuffer /* (b0_spi*) */ mod,
		SugarArg arg, byte[] data, int count) throws ResultException;

	private native static void masterHdWrite(ByteBuffer /* (b0_spi*) */ mod,
		SugarArg arg, byte[] data, int count) throws ResultException;

	private native static void masterHdWriteRead(ByteBuffer /* (b0_spi*) */ mod,
		SugarArg arg, byte[] write_data, int write_count,
		byte[] read_data, int read_count) throws ResultException;

	/**
	 * Native method glue to b0_spi_speed_set()
	 * @param mod pointer to b0_spi
	 */
	private native static void setSpeed(ByteBuffer /* (b0_spi*) */ mod,
		long value) throws ResultException;

	/**
	 * Native method glue to b0_spi_speed_get()
	 * @param mod pointer to b0_spi
	 */
	private native static long getSpeed(ByteBuffer /* (b0_spi*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_spi_active_state_set()
	 */
	private native static void setActiveState(ByteBuffer /* (b0_spi*) */ mod,
		int addr, boolean value) throws ResultException;

	/**
	 * Native method glue to b0_spi_active_state_get()
	 */
	private native static boolean getActiveState(
		ByteBuffer /* (b0_spi*) */ mod, int addr) throws ResultException;
}
