/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.module;

import java.nio.ByteBuffer;

import com.madresistor.box0.module.ModuleInstance;
import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.extra.CUnsignedIntArray;

public class I2c extends ModuleInstance {
	public static class Label {
		public final String sck, sda;
		public Label(String sck, String sda) {
			this.sck = sck;
			this.sda = sda;
		}
	}

	public static class Ref {
		public final double low, high;
		public Ref(double low, double high) {
			this.low = low;
			this.high = high;
		}
	}

	public final static int SM = 0;
	public final static int FM = 1;
	public final static int HS = 2;
	public final static int HS_CLEANUP1 = 3;
	public final static int FMPLUS = 4;
	public final static int UFM = 5;
	public final static int VERSION5 = 6;
	public final static int VERSION6 = 7;

	public final Label label;
	public final Ref ref;
	public final CUnsignedIntArray version;

	public I2c(Device device, int index) throws ResultException {
		super(open(device.getPointer(), index), device);

		ByteBuffer module = getPointer();
		version = propertyVersion(module);
		ref = propertyRef(module);
		label = propertyLabel(module);
	}

	/* TODO: like Dio.Pin class, make Slave class */

	static public class Task {
		public static final int
			TASK_LAST = (1 << 0),
			TASK_WRITE = (0 << 1),
			TASK_READ = (1 << 1),
			TASK_DIR_MASK = (1 << 1);

		public final int flags;
		public final byte addr;
		public final int version;
		public final byte[] data;
		public final int count;

		public Task(int flags, byte addr, int version, byte[] data, int count) {
			this.flags = flags;
			this.addr = addr;
			this.version = version;
			this.data = data;
			this.count = count;
		}

		public Task(int flags, byte addr, byte[] data, int count) {
			this(flags, addr, SM, data, count);
		}

		public Task(int flags, byte addr, int version, byte[] data) {
			this(flags, addr, version, data, data.length);
		}

		public Task(int flags, byte addr, byte[] data) {
			this(flags, addr, data, data.length);
		}
	}

	public static class TaskFailed {
		public final int index;
		public final int ack;
		public TaskFailed(int index, int ack) {
			this.index = index;
			this.ack = ack;
		}
	}

	private final static TaskFailed INVALID_FAILED_TASK =
		new TaskFailed(-1, -1);

	public void masterPrepare() throws ResultException {
		masterPrepare(getPointer());
	}

	public TaskFailed masterStart(Task task) throws ResultException {
		return masterStart(new Task[] { task });
	}

	public TaskFailed masterStart(Task[] tasks) throws ResultException {
		int[] task_failed = new int[2];
		masterStart(getPointer(), tasks, task_failed);

		if (task_failed[0] >= 0 || task_failed[1] >= 0) {
			return new TaskFailed(task_failed[0], task_failed[1]);
		} else {
			return INVALID_FAILED_TASK;
		}
	}

	public void masterStop() throws ResultException {
		masterStop(getPointer());
	}

	public static class SugarArg {
		public final byte addr;
		public final int version;

		public SugarArg(byte addr, int version) {
			this.addr = addr;
			this.version = version;
		}

		public SugarArg(byte addr) {
			this(addr, SM);
		}
	};

	public byte[] masterRead(SugarArg arg, byte[] data, int count)
			throws ResultException {
		masterRead(getPointer(), arg, data, count);
		return data;
	}

	public byte[] masterRead(SugarArg arg, byte[] data)
			throws ResultException {
		masterRead(getPointer(), arg, data, data.length);
		return data;
	}

	public byte[] masterRead(SugarArg arg, int read)
			throws ResultException {
		return masterRead(arg, new byte[read], read);
	}

	public byte[] masterRead(byte addr, int read)
			throws ResultException {
		return masterRead(new SugarArg(addr), read);
	}

	public void masterWrite(SugarArg arg, byte[] data, int count)
			throws ResultException {
		masterWrite(getPointer(), arg, data, count);
	}

	public void masterWrite(SugarArg arg, byte[] data)
			throws ResultException {
		masterWrite(arg, data, data.length);
	}

	public void masterWrite(byte addr, byte[] data)
			throws ResultException {
		masterWrite(new SugarArg(addr), data);
	}

	public byte[] masterWriteRead(SugarArg arg, byte[] write_data,
		int write_count, byte[] read_data, int read_count)
			throws ResultException {
		masterWriteRead(getPointer(), arg, write_data, write_count,
			read_data, read_count);
		return read_data;
	}

	public byte[] masterWriteRead(SugarArg arg, byte[] write, byte[] read)
			throws ResultException {
		return masterWriteRead(arg, write, write.length, read, read.length);
	}

	public byte[] masterWriteRead(SugarArg arg, byte[] write, int read)
			throws ResultException {
		return masterWriteRead(arg, write, write.length, new byte[read], read);
	}

	public byte[] masterWriteRead(byte addr, byte[] write, int read)
			throws ResultException {
		return masterWriteRead(new SugarArg(addr), write, write.length,
				new byte[read], read);
	}

	public byte[] masterWrite8Read(SugarArg arg, byte write, byte[] read_data,
			int read_count) throws ResultException {
		masterWrite8Read(getPointer(), arg, write, read_data, read_count);
		return read_data;
	}

	public byte[] masterWrite8Read(SugarArg arg, byte write, byte[] read)
			throws ResultException {
		return masterWrite8Read(arg, write, read, read.length);
	}

	public byte[] masterWrite8Read(SugarArg arg, byte write, int read)
			throws ResultException {
		return masterWrite8Read(arg, write, new byte[read], read);
	}

	public byte[] masterWrite8Read(byte addr, byte write, int read)
			throws ResultException {
		return masterWrite8Read(new SugarArg(addr), write, read);
	}

	@Override
	public void close() throws ResultException {
		close(getPointer());
		unrefPointer();
	}

	private native static Ref propertyRef(ByteBuffer /* (b0_i2c*) */ mod);

	private native static CUnsignedIntArray propertyVersion(
		ByteBuffer /* (b0_i2c*) */ mod);

	private native static Label propertyLabel(ByteBuffer /* (b0_i2c*) */ mod);

	private native static void masterPrepare(ByteBuffer /* (b0_i2c*) */ mod)
		throws ResultException;

	private native static void masterStart(ByteBuffer /* (b0_i2c*) */ mod,
		Task[] tasks, int[] failed_task /* [0] = Index, [1] = Ack */)
		throws ResultException;

	private native static void masterStop(ByteBuffer /* (b0_i2c*) */ mod)
		throws ResultException;

	private native static void masterRead(ByteBuffer /* (b0_i2c*) */ mod,
		SugarArg arg, byte[] data, int count) throws ResultException;

	private native static void masterWrite(ByteBuffer /* (b0_i2c*) */ mod,
		SugarArg arg, byte[] data, int count) throws ResultException;

	private native static void masterWriteRead(ByteBuffer /* (b0_i2c*) */ mod,
		SugarArg arg, byte[] write_data, int write_count,
		byte[] read_data, int read_count) throws ResultException;

	private native static void masterWrite8Read(ByteBuffer /* (b0_i2c*) */ mod,
		SugarArg arg, byte write, byte[] read_data, int read_count)
			throws ResultException;

	private native static ByteBuffer /* (b0_i2c*) */ open(
		ByteBuffer /* (b0_device*) */ dev, int index)
			throws ResultException;

	private native static void close(ByteBuffer /* (b0_i2c*) */ mod)
			throws ResultException;

	/* TODO: master_slave_detect, master_slaves_detect */
}
