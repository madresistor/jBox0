/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.module;

import java.nio.ByteBuffer;

import com.madresistor.box0.module.ModuleInstance;
import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.extra.CUnsignedLongArray;

/**
 * AOUT Module
 *
 * @author Kuldeep Singh Dhaka
 * @version 0.1
 */
public class Aout extends ModuleInstance {
	public final static int
		CAPAB_FORMAT_2COMPL = (1 << 0),
		CAPAB_FORMAT_BINARY = (0 << 0),

		CAPAB_ALIGN_LSB = (0 << 1),
		CAPAB_ALIGN_MSB = (1 << 1),

		CAPAB_ENDIAN_LITTLE = (0 << 2),
		CAPAB_ENDIAN_BIG = (1 << 2);

	public static class Label {
		public final String[] chan;
		public Label(String[] chan) {
			this.chan = chan;
		}
	}

	public static class Ref {
		public final double low, high;
		public final int type;
		public Ref(double low, double high, int type) {
			this.low = low;
			this.high = high;
			this.type = type;
		}
	}

	public static class BitsizeSpeeds {
		public final int bitsize;
		public final CUnsignedLongArray speed;

		public BitsizeSpeeds(int bitsize, CUnsignedLongArray speed) {
			this.bitsize = bitsize;
			this.speed = speed;
		}
	}

	public final int chanCount;
	public final long bufferSize;
	public final int capab; /* OR between CAPAB_* */
	public final Label label;
	public final Ref ref;
	public final BitsizeSpeeds[] stream, snapshot;

	/** Construct a AOUT module
	 * @param device Parent device
	 * @param index Index
	 */
	public Aout(Device device, int index) throws ResultException {
		super(open(device.getPointer(), index), device);

		ByteBuffer /* (b0_aout*) */ module = getPointer();
		chanCount = propertyChanCount(module);
		bufferSize = propertyBufferSize(module);
		capab = propertyCapab(module);
		label = propertyLabel(module);
		ref = propertyRef(module);
		stream = propertyStream(module);
		snapshot = propertySnapshot(module);
	}

	public void setRepeat(long value) throws ResultException {
		setRepeat(getPointer(), value);
	}

	public long getRepeat() throws ResultException {
		return getRepeat(getPointer());
	}

	public static class BitsizeSpeed {
		public final int bitsize;
		public final long speed;

		public BitsizeSpeed(int bitsize, long speed) {
			this.bitsize = bitsize;
			this.speed = speed;
		}
	}

	public void setBitsizeSpeed(int bitsize, long speed)
			throws ResultException {
		setBitsizeSpeed(getPointer(), bitsize, speed);
	}

	public void setBitsizeSpeed(BitsizeSpeed value)
			throws ResultException {
		setBitsizeSpeed(value.bitsize, value.speed);
	}

	public BitsizeSpeed getBitsizeSpeed() throws ResultException {
		return getBitsizeSpeed(getPointer());
	}

	public void setChanSeq(int[] chan_seq, int length)
			throws ResultException {
		if (length < 1) {
			throw new java.lang.RuntimeException("Length smaller than 1");
		}

		if (chan_seq.length < length) {
			throw new java.lang.RuntimeException("Array size smaller than length");
		}

		setChanSeq(getPointer(), chan_seq, length);
	}

	public void setChanSeq(int[] chan_seq) throws ResultException {
		setChanSeq(chan_seq, chan_seq.length);
	}

	public int[] getChanSeq() throws ResultException {
		return getChanSeq(getPointer());
	}

	/**
	 * Prepare for stream
	 */
	public void streamPrepare() throws ResultException {
		streamPrepare(getPointer());
	}

	/**
	 * Write to stream
	 * @param data Data
	 */
	public void streamWrite(double[] data) throws ResultException {
		streamWrite(getPointer(), data);
	}

	/**
	 * Write to stream
	 * @param data Data
	 */
	public void streamWrite(float[] data) throws ResultException {
		streamWrite(getPointer(), data);
	}

	/**
	 * Start streaming
	 */
	public void streamStart() throws ResultException {
		streamStart(getPointer());
	}

	/**
	 * Stop streaming
	 */
	public void streamStop() throws ResultException {
		streamStop(getPointer());
	}

	/**
	 * Prepare for static mode
	 */
	public void snapshotPrepare() throws ResultException {
		snapshotPrepare(getPointer());
	}

	/**
	 * Start static mode
	 * @param data Data
	 */
	public void snapshotStart(double[] data) throws ResultException {
		snapshotStart(getPointer(), data);
	}

	/**
	 * Start static mode
	 * @param data Data
	 */
	public void snapshotStart(float[] data) throws ResultException {
		snapshotStart(getPointer(), data);
	}

	/**
	 * Stop a ongoing snapshotStart
	 */
	public void snapshotStop() throws ResultException {
		snapshotStop(getPointer());
	}

	/**
	 * Snapshot mode calculation
	 */
	public static class SnapshotCalc {
		public final long count;
		public final long speed;
		public SnapshotCalc(long c, long s) {
			count = c;
			speed = s;
		}
	}

	/**
	 * Calculate the best result for a frequency and bitsize
	 * @param frequency Frequency of the signal to generate
	 * @param bitsize Number of bits per sample
	 * @return the best calculation
	 */
	public SnapshotCalc snapshotCalc(double frequency, int bitsize)
			throws ResultException {
		return snapshotCalc(getPointer(), frequency, bitsize);
	}

	@Override
	public void close() throws ResultException {
		close(getPointer());
		unrefPointer();
	}

	private native static Ref propertyRef(
		ByteBuffer /* (b0_aout*) */ mod);

	private native static BitsizeSpeeds[] propertyStream(
		ByteBuffer /* (b0_aout*) */ mod);

	private native static BitsizeSpeeds[] propertySnapshot(
		ByteBuffer /* (b0_aout*) */ mod);

	private native static int propertyChanCount(
		ByteBuffer /* (b0_aout*) */ mod);

	private native static long propertyBufferSize(
		ByteBuffer /* (b0_aout*) */ mod);

	private native static int propertyCapab(
		ByteBuffer /* (b0_aout*) */ mod);

	private native static Label propertyLabel(
		ByteBuffer /* (b0_aout*) */ mod);

	/**
	 * Native method glue to b0_aout_stream_prepare()
	 * @param mod pointer to b0_aout
	 */
	private native static void streamPrepare(ByteBuffer /* (b0_aout*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_aout_stream_write_double()
	 * @param mod pointer to b0_aout
	 * @param data Data
	 */
	private native static void streamWrite(ByteBuffer /* (b0_aout*) */ mod,
		double[] data) throws ResultException;

	/**
	 * Native method glue to b0_aout_stream_write_float()
	 * @param mod pointer to b0_aout
	 * @param data Data
	 */
	private native static void streamWrite(ByteBuffer /* (b0_aout*) */ mod,
		float[] data) throws ResultException;

	/**
	 * Native method glue to b0_aout_stream_start()
	 * @param mod pointer to b0_aout
	 */
	private native static void streamStart(ByteBuffer /* (b0_aout*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_aout_stream_stop()
	 * @param mod pointer to b0_aout
	 */
	private native static void streamStop(ByteBuffer /* (b0_aout*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_aout_snapshot_prepare()
	 * @param mod pointer to b0_aout
	 */
	private native static void snapshotPrepare(ByteBuffer /* (b0_aout*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_aout_snapshot_start()
	 * @param mod pointer to b0_aout
	 */
	private native static void snapshotStart(ByteBuffer /* (b0_aout*) */ mod,
		double[] data) throws ResultException;

	/**
	 * Native method glue to b0_aout_snapshot_start()
	 * @param mod pointer to b0_aout
	 */
	private native static void snapshotStart(ByteBuffer /* (b0_aout*) */ mod,
		float[] data) throws ResultException;

	/**
	 * Native method glue to b0_aout_snapshot_stop()
	 * @param mod pointer to b0_aout
	 */
	private native static void snapshotStop(ByteBuffer /* (b0_aout*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_aout_snapshot_calc()
	 * @param mod pointer to b0_aout
	 */
	private native static SnapshotCalc snapshotCalc(
		ByteBuffer /* (b0_aout*) */ mod, double freq, int bitsize)
			throws ResultException;

	/**
	 * Native method glue to b0_aout_open()
	 * @param dev pointer to b0_device
	 * @param index Index
	 * @return pointer to b0_aout
	 */
	private native static ByteBuffer /* (b0_aout*) */ open(
		ByteBuffer /* (b0_device*) */ dev, int index) throws ResultException;

	/**
	 * Native method glue to b0_aout_close()
	 * @param mod pointer to b0_aout
	 */
	private native static void close(ByteBuffer /* (b0_aout*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_aout_repeat_set()
	 * @param mod pointer to b0_aout
	 */
	private native static void setRepeat(ByteBuffer /* (b0_aout*) */ mod,
		long value) throws ResultException;

	/**
	 * Native method glue to b0_aout_repeat_get()
	 * @param mod pointer to b0_aout
	 */
	private native static long getRepeat(ByteBuffer /* (b0_aout*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_aout_bitsize_speed_set()
	 * @param mod pointer to b0_aout
	 */
	private native static void setBitsizeSpeed(
		ByteBuffer /* (b0_aout*) */ mod, int bitsize, long speed)
			throws ResultException;

	/**
	 * Native method glue to b0_aout_bitsize_speed_get()
	 * @param mod pointer to b0_aout
	 */
	private native static BitsizeSpeed getBitsizeSpeed(
		ByteBuffer /* (b0_aout*) */ mod) throws ResultException;

	private native static void setChanSeq(ByteBuffer /* (b0_aout*) */ mod,
		int[] chan_seq, int count) throws ResultException;

	private native static int[] getChanSeq(ByteBuffer /* (b0_aout*) */ mod)
		throws ResultException;
}
