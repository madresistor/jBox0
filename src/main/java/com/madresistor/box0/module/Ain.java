/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.module;

import java.nio.ByteBuffer;

import com.madresistor.box0.module.Module;
import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.extra.CUnsignedLongArray;

/**
 * AIN Module
 *
 * @author Kuldeep Singh Dhaka
 * @version 0.1
 */
public class Ain extends ModuleInstance {
	public final static int
		CAPAB_FORMAT_2COMPL = (1 << 0),
		CAPAB_FORMAT_BINARY = (0 << 0),

		CAPAB_ALIGN_LSB = (0 << 1),
		CAPAB_ALIGN_MSB = (1 << 1),

		CAPAB_ENDIAN_LITTLE = (0 << 2),
		CAPAB_ENDIAN_BIG = (1 << 2);

	public static class Label {
		public final String[] chan;
		public Label(String[] chan) {
			this.chan = chan;
		}
	}

	public static class Ref {
		public final double low, high;
		public final int type;
		public Ref(double low, double high, int type) {
			this.low = low;
			this.high = high;
			this.type = type;
		}
	}

	public static class BitsizeSpeeds {
		public final int bitsize;
		public final CUnsignedLongArray speed;

		public BitsizeSpeeds(int bitsize, CUnsignedLongArray speed) {
			this.bitsize = bitsize;
			this.speed = speed;
		}
	}

	public final int chanCount;
	public final long bufferSize;
	public final int capab; /* OR between CAPAB_* */
	public final Label label;
	public final Ref ref;
	public final BitsizeSpeeds[] stream, snapshot;

	/** Construct a AIN module
	 * @param device Parent device
	 * @param index Index
	 */
	public Ain(Device device, int index) throws ResultException {
		super(open(device.getPointer(), index), device);

		ByteBuffer /* (b0_ain*) */ module = getPointer();
		chanCount = propertyChanCount(module);
		bufferSize = propertyBufferSize(module);
		capab = propertyCapab(module);
		label = propertyLabel(module);
		ref = propertyRef(module);
		stream = propertyStream(module);
		snapshot = propertySnapshot(module);
	}

	public static class BitsizeSpeed {
		public final int bitsize;
		public final long speed;

		public BitsizeSpeed(int bitsize, long speed) {
			this.bitsize = bitsize;
			this.speed = speed;
		}
	}

	public void setBitsizeSpeed(int bitsize, long speed)
			throws ResultException {
		setBitsizeSpeed(getPointer(), bitsize, speed);
	}

	public void setBitsizeSpeed(BitsizeSpeed value)
			throws ResultException {
		setBitsizeSpeed(value.bitsize, value.speed);
	}

	public BitsizeSpeed getBitsizeSpeed() throws ResultException {
		return getBitsizeSpeed(getPointer());
	}

	public void setChanSeq(int[] chan_seq, int length)
			throws ResultException {
		if (length < 1) {
			throw new java.lang.RuntimeException("Length smaller than 1");
		}

		if (chan_seq.length < length) {
			throw new java.lang.RuntimeException("Array size smaller than length");
		}

		setChanSeq(getPointer(), chan_seq, length);
	}

	public void setChanSeq(int[] chan_seq) throws ResultException {
		setChanSeq(chan_seq, chan_seq.length);
	}

	public int[] getChanSeq() throws ResultException {
		return getChanSeq(getPointer());
	}

	/**
	 * Prepare module for streaming
	 */
	public void streamPrepare() throws ResultException {
		streamPrepare(getPointer());
	}

	/**
	 * Stream streaming
	 */
	public void streamStart() throws ResultException {
		streamStart(getPointer());
	}

	/**
	 * Read data from stream
	 * <p>
	 * partialRead = true: non-blocking mode
	 * <p>
	 * partialRead = false: blocking mode
	 * @param samples Sample to read to
	 * @param partialRead return partial result, if total data is not available
	 * @return long number of samples readed
	 */
	public long streamRead(double[] samples, boolean partialRead)
			throws ResultException {
		return streamRead(getPointer(), samples, partialRead);
	}

	/** Equivalent to {@code streamRead(data, false) */
	public long streamRead(double[] data) throws ResultException {
		return streamRead(data, false);
	}

	/** same as {@link streamRead(double[], boolean)} */
	public long streamRead(float[] samples, boolean partialRead)
			throws ResultException {
		return streamRead(getPointer(), samples, partialRead);
	}

	/** Equivalent to {@code streamRead(data, false) */
	public long streamRead(float[] data) throws ResultException {
		return streamRead(data, false);
	}

	/**
	 * Stop streaming
	 */
	public void streamStop() throws ResultException {
		streamStop(getPointer());
	}

	/**
	 * Prepare module for static mode
	 */
	public void snapshotPrepare() throws ResultException {
		snapshotPrepare(getPointer());
	}

	/**
	 * start static mode and read data
	 * @param data Data
	 */
	public void snapshotStart(double[] data) throws ResultException {
		snapshotStart(getPointer(), data);
	}

	/** same as {@link snapshotStart(double[])} */
	public void snapshotStart(float[] data) throws ResultException {
		snapshotStart(getPointer(), data);
	}

	/**
	 * Stop an ongoing static operation
	 */
	public void snapshotStop() throws ResultException {
		snapshotStop(getPointer());
	}

	@Override
	public void close() throws ResultException {
		close(getPointer());
		unrefPointer();
	}

	private native static Ref propertyRef(
		ByteBuffer /* (b0_ain*) */ mod);

	private native static BitsizeSpeeds[] propertyStream(
		ByteBuffer /* (b0_ain*) */ mod);

	private native static BitsizeSpeeds[] propertySnapshot(
		ByteBuffer /* (b0_ain*) */ mod);

	private native static int propertyChanCount(
		ByteBuffer /* (b0_ain*) */ mod);

	private native static long propertyBufferSize(
		ByteBuffer /* (b0_ain*) */ mod);

	private native static int propertyCapab(
		ByteBuffer /* (b0_ain*) */ mod);

	private native static Label propertyLabel(
		ByteBuffer /* (b0_ain*) */ mod);

	/**
	 * Native glue method for b0_ain_snapshot_prepare()
	 * @param pointer to b0_ain
	 */
	private native static void snapshotPrepare(
		ByteBuffer /* (b0_ain*) */ mod) throws ResultException;

	/**
	 * Native glue method for b0_ain_snapshot_start()
	 * @param pointer to b0_ain
	 * @param data Data
	 */
	private native static void snapshotStart(ByteBuffer /* (b0_ain*) */ mod,
		float[] data) throws ResultException;

	/**
	 * Native glue method for b0_ain_snapshot_start()
	 * @param pointer to b0_ain
	 * @param data Data
	 */
	private native static void snapshotStart(ByteBuffer /* (b0_ain*) */ mod,
		double[] data) throws ResultException;

	/**
	 * Native glue method for b0_ain_snapshot_stop()
	 * @param pointer to b0_ain
	 */
	private native static void snapshotStop(ByteBuffer /* (b0_ain*) */ mod)
		throws ResultException;

	/**
	 * Native glue method for b0_ain_stream_prepare()
	 * @param pointer to b0_ain
	 */
	private native static void streamPrepare(ByteBuffer /* (b0_ain*) */ mod)
		throws ResultException;

	/**
	 * Native glue method for b0_ain_snapshot_start()
	 * @param pointer to b0_ain
	 */
	private native static void streamStart(ByteBuffer /* (b0_ain*) */  ain)
		throws ResultException;

	/**
	 * Native glue method for b0_ain_stream_read_float()
	 * @param pointer to b0_ain
	 * @param sample Data
	 * @param partialRead, true = non-blocking, false=blocking
	 */
	private native static long streamRead(ByteBuffer /* (b0_ain*) */  ain,
		float[] samples, boolean partialRead) throws ResultException;

	/**
	 * Native glue method for b0_ain_stream_read_double()
	 * @param pointer to b0_ain
	 * @param sample Data
	 * @param partialRead, true = non-blocking, false=blocking
	 */
	private native static long streamRead(ByteBuffer /* (b0_ain*) */  ain,
		double[] samples, boolean partialRead) throws ResultException;

	/**
	 * Native glue method for b0_ain_stream_stop()
	 * @param pointer to b0_ain
	 */
	private native static void streamStop(ByteBuffer /* (b0_ain*) */  ain)
		throws ResultException;

	/**
	 * Native glue method for b0_ain_open()
	 * @param dev pointer to b0_device
	 * @param index Index
	 * @return pointer to b0_ain
	 */
	private native static ByteBuffer /* (b0_ain*) */ open(
		ByteBuffer /* (b0_device*) */ dev, int index)
			throws ResultException;

	/**
	 * Native glue method for b0_ain_close()
	 * @param pointer to b0_ain
	 */
	private native static void close(ByteBuffer /* (b0_ain*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_ain_bitsize_speed_set()
	 * @param mod pointer to b0_ain
	 */
	private native static void setBitsizeSpeed(
		ByteBuffer /* (b0_ain*) */ mod, int bitsize, long speed)
			throws ResultException;

	/**
	 * Native method glue to b0_ain_bitsize_speed_get()
	 * @param mod pointer to b0_ain
	 */
	private native static BitsizeSpeed getBitsizeSpeed(
		ByteBuffer /* (b0_ain*) */ mod) throws ResultException;

	private native static void setChanSeq(ByteBuffer /* (b0_ain*) */ mod,
		int[] chan_seq, int count) throws ResultException;

	private native static int[] getChanSeq(ByteBuffer /* (b0_ain*) */ mod)
		throws ResultException;
}
