/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.module;

import java.nio.ByteBuffer;
import java.util.Iterator;

import com.madresistor.box0.module.ModuleInstance;
import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;

public class Dio extends ModuleInstance implements Iterable<Dio.Pin> {
	public static class Label {
		public final String[] pin;

		public Label(String[] pin) {
			this.pin = pin;
		}
	}

	public static class Ref {
		public final double low, high;
		public final int type;
		public Ref(double low, double high, int type) {
			this.low = low;
			this.high = high;
			this.type = type;
		}
	}

	public final int pinCount;
	public final int capab;
	public final Label label;
	public final Ref ref;

	public Dio(Device device, int index) throws ResultException {
		super(open(device.getPointer(), index), device);

		ByteBuffer /* (b0_dio*) */ module = getPointer();
		pinCount = propertyPinCount(module);
		capab = propertyCapab(module);
		label = propertyLabel(module);
		ref = propertyRef(module);
	}

	public class PinIterator implements Iterator<Pin> {
		private int currentIndex = 0;

		@Override
		public boolean hasNext() {
			return (currentIndex < Dio.this.pinCount);
		}

		@Override
		public Pin next() {
			return new Pin(currentIndex++);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Cannot remove Pin");
		}
	}

	@Override
	public Iterator<Pin> iterator() {
		return new PinIterator();
	}

	public class Pin {
		public final int m_index;

		public Pin(int i) throws IndexOutOfBoundsException {
			if (i >= 0 && i < Dio.this.pinCount) {
				m_index = i;
			} else {
				throw new IndexOutOfBoundsException("index not a valid pin number");
			}
		}

		public void setDir(boolean dir) throws ResultException {
			Dio.this.setDir(m_index, dir);
		}

		public void setValue(boolean value) throws ResultException {
			Dio.this.setValue(m_index, value);
		}

		public boolean getDir() throws ResultException {
			return Dio.this.getDir(m_index);
		}

		public boolean getValue() throws ResultException {
			return Dio.this.getValue(m_index);
		}

		public void toggle() throws ResultException {
			Dio.this.toggle(m_index);
		}

		public void high() throws ResultException {
			Dio.this.high(m_index);
		}

		public void low() throws ResultException {
			Dio.this.low(m_index);
		}

		public void input() throws ResultException {
			Dio.this.input(m_index);
		}

		public void output() throws ResultException {
			Dio.this.output(m_index);
		}
	}

	/* Pin dir */
	public static final boolean INPUT = false;
	public static final boolean OUTPUT = true;

	/* Output pin value */
	public static final boolean LOW = false;
	public static final boolean HIGH = true;

	/* HiZ value */
	public static final boolean DISABLE = false;
	public static final boolean ENABLE = true;

	/* <single> */
	public void setDir(int index, boolean dir) throws ResultException {
		setDir(getPointer(), index, dir);
	}

	public void setValue(int index, boolean value) throws ResultException {
		setValue(getPointer(), index, value);
	}

	public boolean getDir(int index) throws ResultException {
		return getDir(getPointer(), index);
	}

	public boolean getValue(int index) throws ResultException {
		return getValue(getPointer(), index);
	}

	public void toggle(int index) throws ResultException {
		toggle(getPointer(), index);
	}

	public void setHiz(int index, boolean value) throws ResultException {
		setHiz(getPointer(), index, value);
	}

	public boolean getHiz(int index, boolean value) throws ResultException {
		return getHiz(getPointer(), index);
	}

	/* helper function */
	public void high(int index) throws ResultException {
		setValue(index, HIGH);
	}

	public void low(int index) throws ResultException {
		setValue(index, LOW);
	}

	public void input(int index) throws ResultException {
		setDir(index, INPUT);
	}

	public void output(int index) throws ResultException {
		setDir(index, OUTPUT);
	}

	/* TODO: support Multiple and All commands */

	@Override
	public void close() throws ResultException {
		close(getPointer());
		unrefPointer();
	}

	public void basicPrepare() throws ResultException {
		basicPrepare(getPointer());
	}

	public void basicStart() throws ResultException {
		basicStart(getPointer());
	}

	public void basicStop() throws ResultException {
		basicStop(getPointer());
	}

	private native static void setDir(ByteBuffer /* (b0_dio*) */ mod,
		int index, boolean dir) throws ResultException;

	private native static void setValue(ByteBuffer /* (b0_dio*) */ mod,
		int index, boolean value) throws ResultException;

	private native static boolean getDir(ByteBuffer /* (b0_dio*) */ mod,
		int index) throws ResultException;

	private native static boolean getValue(ByteBuffer /* (b0_dio*) */ mod,
		int index) throws ResultException;

	private native static void toggle(ByteBuffer /* (b0_dio*) */ mod,
		int index) throws ResultException;

	private native static void setHiz(ByteBuffer /* (b0_dio*) */ mod,
		int index, boolean value) throws ResultException;

	private native static boolean getHiz(ByteBuffer /* (b0_dio*) */ mod,
		int index) throws ResultException;

	private native static Ref propertyRef(ByteBuffer /* (b0_dio*) */ mod);
	private native static int propertyPinCount(ByteBuffer /* (b0_dio*) */ mod);
	private native static int propertyCapab(ByteBuffer /* (b0_dio*) */ mod);
	private native static Label propertyLabel(ByteBuffer /* (b0_dio*) */ mod);

	private native static ByteBuffer /* (b0_dio*) */ open(
		ByteBuffer /* (b0_device*) */ dev, int index) throws ResultException;

	private native static void close(ByteBuffer /* (b0_dio*) */ mod)
		throws ResultException;

	private native static void basicPrepare(ByteBuffer /* (b0_dio*) */ mod)
		throws ResultException;

	private native static void basicStart(ByteBuffer /* (b0_dio*) */ mod)
		throws ResultException;

	private native static void basicStop(ByteBuffer /* (b0_dio*) */ mod)
		throws ResultException;
}
