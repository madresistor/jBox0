/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.module;

import java.nio.ByteBuffer;

import com.madresistor.box0.module.ModuleInstance;
import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.extra.CUnsignedIntArray;
import com.madresistor.box0.extra.CUnsignedLongArray;

public class Pwm extends ModuleInstance {
	public static class Label {
		public final String[] pin;

		public Label(String[] pin) {
			this.pin = pin;
		}
	}

	public static class Ref {
		public final double low, high;
		public final int type;
		public Ref(double low, double high, int type) {
			this.low = low;
			this.high = high;
			this.type = type;
		}
	}

	public final int pinCount;
	public final Label label;
	public final CUnsignedIntArray bitsize;
	public final CUnsignedLongArray speed;
	public final Ref ref;

	/**
	 * Construct a PWM module from Device
	 * @param device Device
	 * @param index Module index
	 */
	public Pwm(Device device, int index) throws ResultException {
		super(open(device.getPointer(), index), device);

		ByteBuffer module = getPointer();
		pinCount = propertyPinCount(module);
		label = propertyLabel(module);
		bitsize = propertyBitsize(module);
		speed = propertySpeed(module);
		ref = propertyRef(module);
	}

	/**
	 * prepare PWM for Output
	 */
	public void outputPrepare() throws ResultException {
		outputPrepare(getPointer());
	}

	/**
	 * Start PWM in Output
	 */
	public void outputStart() throws ResultException {
		outputStart(getPointer());
	}

	/**
	 * Stop PWM in Output
	 */
	public void outputStop() throws ResultException {
		outputStop(getPointer());
	}

	/**
	 * Set Width value of channel
	 * @param index Channel index
	 * @param value Channel width value
	 */
	public void setWidth(int index, long value) throws ResultException {
		setWidth(getPointer(), index, value);
	}

	/**
	 * Get Width value of channel
	 * @param index Channel
	 * @return Channel current width value
	 */
	public long setWidth(int index) throws ResultException {
		return getWidth(getPointer(), index);
	}

	/**
	 * Set the period value
	 * @param value Period value
	 */
	public void setPeriod(long value) throws ResultException {
		setPeriod(getPointer(), value);
	}

	/**
	 * Get the period
	 * @return period value
	 */
	public long getPeriod() throws ResultException {
		return getPeriod(getPointer());
	}

	/**
	 * Set the bitsize value
	 * @param value Bitsize value
	 */
	public void setBitsize(int value) throws ResultException {
		setBitsize(getPointer(), value);
	}

	/**
	 * Get the bitsize
	 * @return bitsize value
	 */
	public int getBitsize() throws ResultException {
		return getBitsize(getPointer());
	}

	/**
	 * Set the speed value
	 * @param value Period value
	 */
	public void setSpeed(long value) throws ResultException {
		setSpeed(getPointer(), value);
	}

	/**
	 * Get the speed
	 * @return speed value
	 */
	public long getSpeed() throws ResultException {
		return getSpeed(getPointer());
	}

	public static class OutputCalc {
		public final long speed, period;
		public OutputCalc(long speed, long period) {
			this.speed = speed;
			this.period = period;
		}

		public long calcWidth(double duty_cycle) {
			return Pwm.outputCalcWidth(this.period, duty_cycle);
		}

		public double calcDutyCycle(long width) {
			return Pwm.outputCalcDutyCycle(this.period, width);
		}

		public double calcFreq() {
			return Pwm.outputCalcFreq(this.speed, this.period);
		}
	}

	/**
	 * Calculate speed and period for a given frequency under tolerable error.
	 * @param freq Frequency
	 * @param speed Store Resultant speed
	 * @param period Store Resultant period
	 * @param max_error Maximum error
	 * @param best_result if true, will perform a exaustive search for lest error result
	 */
	public OutputCalc outputCalc(int bitsize, double freq, double max_error,
			boolean best_result) throws ResultException {
		return outputCalc(getPointer(), bitsize, freq, max_error, best_result);
	}

	/**
	 * same as {@literal outputCalc(freq, max_error, true)}
	 */
	public OutputCalc outputCalc(int bitsize, double freq, double max_error)
					throws ResultException {
		return outputCalc(bitsize, freq, max_error, true);
	}

	@Override
	public void close() throws ResultException {
		close(getPointer());
		unrefPointer();
	}

	/**
	 * Calculate width value from period and duty cycle.
	 * @param[in] period Period
	 * @param[in] duty_cycle Duty Cycle
	 * @note duty_cycle denoted in term of percentage. (floating point value)
	 * @note duty_cycle should be greater than 0
	 * @note duty_cycle should be smaller than 100
	 * @note https://en.wikipedia.org/wiki/Pulse-width_modulation
	 * @return Width value
	 */
	public static long outputCalcWidth(long period, double duty_cycle) {
		return (long)((period * duty_cycle) / 100.0);
	}

	/**
	 * Calculate duty cycle from period and duty_cycle
	 * @param[in] period Period
	 * @param[in] width Width
	 * @return Duty cycle
	 */
	public static double outputCalcDutyCycle(long period, long width) {
		return (width * 100.0) / period;
	}

	/**
	 * Calculate frequency from period and speed
	 * @param[in] speed Speed
	 * @param[in] period Period
	 * @return Frequency
	 */
	public static double outputCalcFreq(long speed, long period) {
		return ((double) speed) / ((double) period);
	}

	/**
	 * Calculate error.
	 * @param[in] required_freq Required frequency (user need)
	 * @param[in] calc_freq Calculated frequency (user can have)
	 * @return Error (in percentage)
	 * @note *Relative error*
	 */
	public static double outputCalcFreqError(double required_freq, double calc_freq) {
		return (Math.abs(required_freq - calc_freq) * 100.0) / required_freq;
	}

	private native static Ref propertyRef(
		ByteBuffer /* (b0_pwm*) */ mod);

	private native static CUnsignedIntArray propertyBitsize(
		ByteBuffer /* (b0_pwm*) */ mod);

	private native static CUnsignedLongArray propertySpeed(
		ByteBuffer /* (b0_pwm*) */ mod);

	private native static Label propertyLabel(
		ByteBuffer /* (b0_pwm*) */ mod);

	private native static int propertyPinCount(
		ByteBuffer /* (b0_pwm*) */ mod);

	/**
	 * Native method glue to b0_pwm_output_calc()
	 * @param mod pointer to b0_pwm
	 */
	private native static OutputCalc outputCalc(
		ByteBuffer /* (b0_pwm*) */ mod, int bitsize, double frequency,
		double max_error, boolean best_result) throws ResultException;

	/**
	 * Native method glue to b0_pwm_period_set()
	 * @param mod pointer to b0_pwm
	 */
	private native static void setPeriod(ByteBuffer /* (b0_pwm*) */ mod,
		long value) throws ResultException;

	/**
	 * Native method glue to b0_pwm_period_get()
	 * @param mod pointer to b0_pwm
	 */
	private native static long getPeriod(ByteBuffer /* (b0_pwm*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_pwm_bitsize_set()
	 * @param mod pointer to b0_pwm
	 */
	private native static void setBitsize(ByteBuffer /* (b0_pwm*) */ mod,
		int value) throws ResultException;

	/**
	 * Native method glue to b0_pwm_bitsize_get()
	 * @param mod pointer to b0_pwm
	 */
	private native static int getBitsize(ByteBuffer /* (b0_pwm*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_pwm_speed_set()
	 * @param mod pointer to b0_pwm
	 */
	private native static void setSpeed(ByteBuffer /* (b0_pwm*) */ mod,
		long value) throws ResultException;

	/**
	 * Native method glue to b0_pwm_speed_get()
	 * @param mod pointer to b0_pwm
	 */
	private native static long getSpeed(ByteBuffer /* (b0_pwm*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_pwm_width_set()
	 * @param mod pointer to b0_pwm
	 */
	private native static void setWidth(ByteBuffer /* (b0_pwm*) */ mod,
		int index,  long value) throws ResultException;

	/**
	 * Native method glue to b0_pwm_width_get()
	 * @param mod pointer to b0_pwm
	 */
	private native static long getWidth(ByteBuffer /* (b0_pwm*) */ mod,
		int index) throws ResultException;

	/**
	 * Native method glue to b0_pwm_output_prepare()
	 * @param mod pointer to b0_pwm
	 */
	private native static void outputPrepare(ByteBuffer /* (b0_pwm*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_pwm_output_start()
	 * @param mod pointer to b0_pwm
	 */
	private native static void outputStart(ByteBuffer /* (b0_pwm*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_pwm_output_stop()
	 * @param mod pointer to b0_pwm
	 */
	private native static void outputStop(ByteBuffer /* (b0_pwm*) */ mod)
		throws ResultException;

	/**
	 * Native method glue to b0_pwm_output_open()
	 * @param mod pointer to b0_pwm
	 */
	private native static ByteBuffer /* (b0_pwm*) */ open(
		ByteBuffer /* (b0_device*) */ dev, int index) throws ResultException;

	/**
	 * Native method glue to b0_pwm_close()
	 * @param mod pointer to b0_pwm
	 */
	private native static void close(ByteBuffer /* (b0_pwm*) */ mod)
		throws ResultException;
}
