/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.module;

import java.nio.ByteBuffer;

import com.madresistor.box0.extra.Base;
import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.Ain;
import com.madresistor.box0.module.Aout;
import com.madresistor.box0.module.Spi;
import com.madresistor.box0.module.I2c;
import com.madresistor.box0.module.Spi;
import com.madresistor.box0.module.Pwm;
import com.madresistor.box0.module.Dio;

public class Module extends Base {
	public final Device device;
	public final int type;
	public final int index;
	public final String name;

	public final static int DIO = 1;
	public final static int AOUT = 2;
	public final static int AIN = 3;
	public final static int SPI = 4;
	public final static int I2C = 5;
	public final static int PWM = 6;
	public final static int UNIO = 7;

	public Module(ByteBuffer mod, Device dev) {
		super(mod);
		device = dev;
		index = getIndex(mod);
		type = getType(mod);
		name = getName(mod);
	}

	public boolean openable() throws ResultException {
		return openable(getPointer());
	}

	public ModuleInstance open() throws ResultException, UnsupportedOperationException {
		switch(type) {
		case DIO: return new Dio(device, index);
		case AOUT: return new Aout(device, index);
		case AIN: return new Ain(device, index);
		case SPI: return new Spi(device, index);
		case I2C: return new I2c(device, index);
		case PWM: return new Pwm(device, index);
		}

		throw new UnsupportedOperationException("Type not supported " + type);
	}

	private native static int getIndex(ByteBuffer /* (b0_module*) */ mod);
	private native static int getType(ByteBuffer /* (b0_module*) */ mod);
	private native static String getName(ByteBuffer /* (b0_module*) */ mod);
	private native static boolean openable(ByteBuffer /* (b0_module*) */ mod)
		throws ResultException;
};
