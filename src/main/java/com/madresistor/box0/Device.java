/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0;

import java.nio.ByteBuffer;
import java.lang.NullPointerException;

import com.madresistor.box0.extra.Base;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.Ain;
import com.madresistor.box0.module.Aout;
import com.madresistor.box0.module.Dio;
import com.madresistor.box0.module.I2c;
import com.madresistor.box0.module.Spi;
import com.madresistor.box0.module.Pwm;

/* TODO: Iterator over Modules */

/**
 * Box0 Device
 *
 * @author Kuldeep Singh Dhaka
 * @version 0.1
 *
 * @see com.madresistor.box0.Usb
 */
public class Device extends Base implements Base.Close {
	/** Device Name */
	final public String name;

	/** Manufacturer Name */
	final public String manuf;

	/** Serial Number */
	final public String serial;

	/**
	 * Construct Device from b0_device pointer
	 * <p>
	 * <b>DO NOT</b> use this unless you do not know what you are doing.
	 * Probebly you are looking for {@link com.madresistor.box0.backend.Usb#openSupported()}
	 * @param device C Pointer to b0_device
	 */
	public Device(ByteBuffer /* (b0_device*) */ device) {
		super(device);

		name = getName(device);
		manuf = getManuf(device);
		serial = getSerial(device);
	}

	/**
	 * Close the device.
	 */
	public void close() throws ResultException {
		close(getPointer());
		unrefPointer();
	}

	/**
	 * Get a AIN module using index
	 *
	 * @param index index of the module
	 * @return Ain Module
	 */
	public Ain ain(int index) throws ResultException {
		return new Ain(this, index);
	}

	/** Equivalent to {@code ain(0)} */
	public Ain ain() throws ResultException {
		return ain(0);
	}

	/**
	 * Get a AOUT module using index
	 *
	 * @param index index of the module
	 * @return Aout Module
	 */
	public Aout aout(int index) throws ResultException {
		return new Aout(this, index);
	}

	/** Equivalent to {@code aout(0)} */
	public Aout aout() throws ResultException {
		return aout(0);
	}

	/**
	 * Get a SPI module using index
	 *
	 * @param index index of the module
	 * @return Spi Module
	 */
	public Spi spi(int index) throws ResultException {
		return new Spi(this, index);
	}

	/** Equivalent to {@code spi(0)} */
	public Spi spi() throws ResultException {
		return spi(0);
	}

	/**
	 * Get a I2C module using index
	 *
	 * @param index index of the module
	 * @return I2c Module
	 */
	public I2c i2c(int index) throws ResultException {
		return new I2c(this, index);
	}

	/** Equivalent to {@code i2c(0)} */
	public I2c i2c() throws ResultException {
		return i2c(0);
	}

	/**
	 * Get a DIO module using index
	 *
	 * @param index index of the module
	 * @return Dio Module
	 */
	public Dio dio(int index) throws ResultException {
		return new Dio(this, index);
	}

	/** Equivalent to {@code dio(0)} */
	public Dio dio() throws ResultException {
		return dio(0);
	}

	/**
	 * Get a PWM module using index
	 *
	 * @param index index of the module
	 * @return Pwm Module
	 */
	public Pwm pwm(int index) throws ResultException {
		return new Pwm(this, index);
	}

	/** Equivalent to {@code pwm(0)} */
	public Pwm pwm() throws ResultException {
		return pwm(0);
	}

	/**
	 * Ping device.
	 * If the operation fails,
	 *   there is some communication problem with the device.
	 */
	public void ping() throws ResultException {
		ping(getPointer());
	}

	public static final int
		/** Log no message to stderr */
		LOG_NONE = 0,

		/** Log message with log_level of ERROR to stderr */
		LOG_ERROR = 1,

		/** Log message with log_level of {WARN, ERROR} to stderr */
		LOG_WARN = 2,

		/** Log message with log_level of {WARN, ERROR, INFO} to stderr */
		LOG_INFO = 3,

		/** Log message with log_level of {WARN, ERROR, INFO, DEBUG} to stderr */
		LOG_DEBUG = 4;

	/**
	 * Set log level for Device.
	 * <p>
	 * NOTE: This method will throw ResultException,
	 *   when configurable log level facility is disabled by libbox0
	 * </p>
	 *
	 * @param level Log level to set
	 */
	public void log(int level) throws ResultException {
		log(getPointer(), level);
	}

	/**
	 * Native glue method for b0_device_close()
	 * @param device pointer to b0_device
	 */
	private native static void close(ByteBuffer dev) throws ResultException;

	/**
	 * Native glue method for b0_device_log()
	 * @param device pointer to b0_device
	 */
	private native static void log(ByteBuffer dev, int log_level)
		throws ResultException;

	/**
	 * Native Get "manuf" member of b0_device
	 * @param device pointer to b0_device
	 */
	private native static String getManuf(ByteBuffer dev);

	/**
	 * Native Get "name" member of b0_device
	 * @param device pointer to b0_device
	 */
	private native static String getName(ByteBuffer dev);

	/**
	 * Native Get "serial" member of b0_device
	 * @param device pointer to b0_device
	 */
	private native static String getSerial(ByteBuffer dev);

	/**
	 * Native glue method for b0_device_ping()
	 * @param device pointer to b0_device
	 */
	private native static void ping(ByteBuffer dev)
		throws ResultException;
}
