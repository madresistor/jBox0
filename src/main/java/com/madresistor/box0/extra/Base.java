/*
 * This file is part of jBase.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBase.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.extra;

import java.nio.ByteBuffer;

import com.madresistor.box0.ResultException;
import com.madresistor.box0.extra.JniLoader;

/**
 * Basic things management
 *
 * @author Kuldeep Singh Dhaka
 * @version 0.1
 */
public abstract class Base {

	/** pointer to Base C object */
	private ByteBuffer m_pointer;

	/**
	 * Construct a Base object from native memory
	 * @param pointer Pointer to C memory
	 */
	public Base(ByteBuffer pointer) {
		m_pointer = pointer;
	}

	/**
	 * Get a reference to Internal C pointer of the object
	 *
	 * @return C Pointer
	 */
	public ByteBuffer getPointer() {
		return m_pointer;
	}

	/**
	 * Unreference the internal C pointer.
	 * Internal pointer is set to null after this point.
	 * @throws IllegalStateException when internal pointer is null (already unreferenced)
	 */
	protected void unrefPointer() {
		if (m_pointer == null) {
			throw new IllegalStateException("Internal pointer is null");
		}

		m_pointer = null;
	}

	/**
	 * Closable Base object
	 */
	public interface Close {
		/**
		 * Close the object.
		 * Note: close will be performed in libbox0 C object.
		 */
		public void close() throws ResultException;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Base) {
			Base _obj = (Base) obj;
			return equals(getPointer(), _obj.getPointer());
		}

		return super.equals(obj);
	}

	/**
	 * Match weather two native memory location are same.
	 * @param ptr1 Pointer
	 * @param ptr2 Pointer
	 * @return true if point to same memory
	 */
	private static native boolean equals(ByteBuffer ptr1, ByteBuffer ptr2);

	static {
		JniLoader.glue();
	}
}
