/*
 * This file is part of jBox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.extra;

import java.nio.ByteBuffer;
import java.util.Iterator;

public class CUnsignedLongArray implements Iterable<Long> {
	public final long length;
	public final ByteBuffer pointer;

	public CUnsignedLongArray(ByteBuffer pointer, long length) {
		this.pointer = pointer;
		this.length = length;
	}

	@Override
	public Iterator<Long> iterator() {
		return new Iterator<Long> () {
			private long currentIndex = 0;

			@Override
			public boolean hasNext() {
				return (currentIndex < length);
			}

			@Override
			public Long next() {
				return getValue(pointer, currentIndex++);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("Not supported");
			}
		};
	}

	public long get(long index) throws ArrayIndexOutOfBoundsException {
		if (index < 0) {
			throw new ArrayIndexOutOfBoundsException("Negative index not supported");
		} else if (index >= length) {
			throw new ArrayIndexOutOfBoundsException("Index not less than length");
		}

		return getValue(pointer, index);
	}

	private static native long getValue(ByteBuffer pointer, long index);
}
