/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0;

/**
 * Handling libbox0 result code.
 * <p>
 * only negative values throw exception.
 *
 * @author Kuldeep Singh Dhaka
 * @version 0.1
 */
public class ResultException extends Exception {
	/** result code of libbox0 */
	private final int m_value;

	/**
	 * Construct a ResultException from libbox0 result code
	 *
	 * @param code libbox0 C result code
	 */
	public ResultException(int value) {
		m_value = value;
	}

	/**
	 * Get an explination string of the result code
	 *
	 * @return String explaining the result code
	 */
	public String explain() {
		return explain(m_value);
	}

	public String toString() {
		return name(m_value);
	}

	/**
	 * Get the actual result code value.
	 * this code will be negative.
	 * as per libbox0, negative values are only consider error.
	 *
	 * @return integer value of libbox0 result code
	 */
	public int getValue() {
		return m_value;
	}

	/**
	 * Native glue method for b0_result_explain()
	 * @param rc Result code
	 * @return Explain string
	 */
	public native static String explain(int rc);

	/**
	 * Native glue method for b0_result_name()
	 * @param rc Result code
	 * @param Name string
	 */
	public native static String name(int rc);
}
