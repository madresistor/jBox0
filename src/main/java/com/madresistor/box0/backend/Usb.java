/*
 * This file is part of jBox0.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.backend;

import java.nio.ByteBuffer;
import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.extra.JniLoader;

/**
 * USB Backend
 *
 * @author Kuldeep Singh Dhaka
 * @since 0.1
 */
public final class Usb {
	/* Prevent sub-classing or instantiation.
	 * Ref: http://stackoverflow.com/a/7486111 */
	private Usb() {}

	/**
	 * Try to open any supported device.
	 * This is the most used function.
	 * @return Box0 device
	 */
	public native static Device openSupported() throws ResultException;

	/**
	 * Open a device using VID and PID.
	 * <p>
	 * This will open the first matching device.
	 * @param vid USB VID
	 * @param pid USB PID
	 * @return Box0 equivalent device
	 */
	public native static Device openVidPid(int vid, int pid) throws ResultException;

	/**
	 * Open a device using libusb_device
	 * @param usbd pointer to libusb_device
	 * @param usbc pointer to libusb_context (that was used for libusb_device)
	 * @return Box0 equivalent device
	 */
	public native static Device open(ByteBuffer /* (libusb_device*) */ usbd,
		ByteBuffer /* (libusb_context) */ usbc) throws ResultException;

	/**
	 * Open a device using libusb device handle
	 * @param usbdh pointer to libusb_device_handle
	 * @param usbc pointer to libusb_context (that was used for usbdh)
	 * @return {@link com.madresistor.box0.Device}
	 */
	public native static Device openHandle(
		ByteBuffer /* (libusb_device_handle*) */ usbdh,
		ByteBuffer /* (libusb_context) */ usbc) throws ResultException;

	static {
		JniLoader.glue();
	}
}
