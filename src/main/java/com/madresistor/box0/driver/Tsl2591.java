/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.driver;

import java.nio.ByteBuffer;

import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.I2c;
import com.madresistor.box0.driver.Driver;

public class Tsl2591 extends Driver {
	public Tsl2591(I2c module) throws ResultException {
		this(module, null);
	}

	public Tsl2591(I2c module, Config config) throws ResultException {
		super(open(module.getPointer(), config));
	}

	public void close() throws ResultException { close(getPointer()); }

	public double read() throws ResultException { return read(getPointer()); }

	public void setGain(Gain gain) throws ResultException { gainSet(getPointer(), gain.getValue()); }
	public void setInteg(Integ integ) throws ResultException { integSet(getPointer(), integ.getValue()); }

	public static enum Gain {
		LOW(0),
		MEDIUM(1),
		HIGH(2),
		MAX(3);

		private final int m_value;
		private Gain(int value) { m_value = value; }
		public int getValue() { return m_value; }

		static public Gain reverseLookup(int value) {
			switch(value) {
			case 0:
			return LOW;
			case 1:
			return MEDIUM;
			case 2:
			return HIGH;
			case 3:
			return MAX;
			}

			throw new IllegalArgumentException("value out of range");
		}
	};

	public static enum Integ {
		INTEG_100MS(0),
		INTEG_200MS(1),
		INTEG_300MS(2),
		INTEG_400MS(3),
		INTEG_500MS(4),
		INTEG_600MS(5);

		private final int m_value;
		private Integ(int value) { m_value = value; }
		public int getValue() { return m_value; }

		static public Integ reverseLookup(int value) {
			switch(value) {
			case 0:
			return INTEG_100MS;
			case 1:
			return INTEG_200MS;
			case 2:
			return INTEG_300MS;
			case 3:
			return INTEG_400MS;
			case 4:
			return INTEG_500MS;
			case 5:
			return INTEG_600MS;
			}

			throw new IllegalArgumentException("value out of range");
		}
	};

	public static class Config {
		public double DGF;
		public double CoefB, CoefC, CoefD;
	};

	private final static native ByteBuffer open(ByteBuffer /* (b0_i2c*) */ module, Config config) throws ResultException;
	private final static native void close(ByteBuffer /* (b0_tsl2591*) */ driver) throws ResultException;
	private final static native double read(ByteBuffer /* (b0_tsl2591*) */ driver) throws ResultException;
	private final static native void integSet(ByteBuffer /* (b0_tsl2591*) */ driver, int integ) throws ResultException;
	private final static native void gainSet(ByteBuffer /* (b0_tsl2591*) */ driver, int gain) throws ResultException;
}
