/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.driver;

import java.nio.ByteBuffer;

import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.Spi;
import com.madresistor.box0.driver.Driver;

public class Max31855 extends Driver {
	public Max31855(Spi module, int addr) throws ResultException {
		super(open(module.getPointer(), addr));
	}

	public void close() throws ResultException { close(getPointer()); }

	public double read() throws ResultException {
		return read(getPointer());
	}

	private final static native ByteBuffer open(ByteBuffer /* (b0_spi*) */ module, int addr) throws ResultException;
	private final static native void close(ByteBuffer /* (b0_max31855*) */ driver) throws ResultException;
	private final static native double read(ByteBuffer /* (b0_max31855*) */ driver) throws ResultException;
}
