/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.driver;

import java.nio.ByteBuffer;

import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.I2c;
import com.madresistor.box0.driver.Driver;

public class Lm75 extends Driver {
	public Lm75(I2c module, boolean A2, boolean A1, boolean A0) throws ResultException {
		super(open(module.getPointer(), A2, A1, A0));
	}

	public void close() throws ResultException { close(getPointer()); }

	public double read() throws ResultException {
		return read(getPointer());
	}

	public double read16() throws ResultException {
		return read16(getPointer());
	}

	private final static native ByteBuffer open(ByteBuffer /* (b0_i2c*) */ module, boolean A2, boolean A1, boolean A0) throws ResultException;
	private final static native void close(ByteBuffer /* (b0_lm75*) */ driver) throws ResultException;
	private final static native double read(ByteBuffer /* (b0_lm75*) */ driver) throws ResultException;
	private final static native double read16(ByteBuffer /* (b0_lm75*) */ driver) throws ResultException;
}
