/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.driver;

import java.nio.ByteBuffer;

import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.I2c;
import com.madresistor.box0.driver.Driver;

/**
 * BMP180 driver
 *
 * @author Kuldeep Singh Dhaka
 * @since 0.1
 */
public class Bmp180 extends Driver {

	/**
	 * BMP180 driver
	 * @param module I2C module
	 */
	public Bmp180(I2c module) throws ResultException {
		super(open(module.getPointer()));
	}

	public void close() throws ResultException {
		close(getPointer());
	}

	public static enum OverSample {
		OVER_SAMP_1(0),
		OVER_SAMP_2(1),
		OVER_SAMP_4(2),
		OVER_SAMP_8(3);

		/** oversample value */
		private final int m_value;

		private OverSample(int value) {
			m_value = value;
		}

		public int getValue() {
			return m_value;
		}

		/**
		 * Reverse lookup the value
		 * @param value libbox0 bmp180 oversample value
		 */
		static public OverSample reverseLookup(int value) {
			switch(value) {
			case 0:
			return OVER_SAMP_1;
			case 1:
			return OVER_SAMP_2;
			case 2:
			return OVER_SAMP_4;
			case 3:
			return OVER_SAMP_8;
			}

			throw new IllegalArgumentException("value out of range");
		}
	};

	/**
	 * Set over sample value
	 * @param overSample Over sample
	 */
	public void setOverSample(OverSample overSample) throws ResultException {
		overSampleSet(getPointer(), overSample.getValue());
	}

	/**
	 * Read pressure value
	 * @return pressure
	 */
	public double read() throws ResultException {
		return read(getPointer());
	}

	/* read pressure = values[0] and temp = values[1] */
	/**
	 * Read pressure and temprature value
	 * @param values value to read into.
	 *   Store:
	 *     [0] = pressure,
	 *     [1] = temperature
	 */
	public void read(double[] values) throws ResultException,
					NullPointerException, IllegalArgumentException {
		if (values == null) {
			throw new NullPointerException("values is null");
		}

		if (!(values.length >= 2)) {
			throw new NullPointerException("values length should atleast be 2");
		}

		read(getPointer(), values);
	}

	/**
	 * native glue method for b0_bmp180_open()
	 * @param driver pointer to b0_i2c
	 */
	private final static native ByteBuffer /* (b0_bmp180*) */ open(
		ByteBuffer /* (b0_i2c*) */ module) throws ResultException;

	/**
	 * native glue method for b0_bmp180_close()
	 * @param driver pointer to b0_bmp180
	 */
	private final static native void close(
		ByteBuffer /* (b0_bmp180*) */ driver) throws ResultException;

	/**
	 * native glue method for b0_bmp180_over_samp_set()
	 * @param driver pointer to b0_bmp180
	 */
	private final static native void overSampleSet(
		ByteBuffer /* (b0_bmp180*) */ driver,
		int overSample) throws ResultException;

	/**
	 * native glue method for b0_bmp180_read()
	 * @param driver pointer to b0_bmp180
	 */
	private final static native double read(
		ByteBuffer /* (b0_bmp180*) */ driver) throws ResultException;

	/**
	 * native glue method for b0_bmp180_read()
	 * @param driver pointer to b0_bmp180
	 */
	private final static native void read(
		ByteBuffer /* (b0_bmp180*) */ driver,
		double[] values) throws ResultException;
}
