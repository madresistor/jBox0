/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.driver;

import java.nio.ByteBuffer;

import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.I2c;
import com.madresistor.box0.driver.Driver;

/**
 * ADXL345 driver
 *
 * @author Kuldeep Singh Dhaka
 * @since 0.1
 */
public class Adxl345 extends Driver {

	/**
	 * ADXL345 object using module and ALT_ADDRESS bit
	 * @param module I2C module
	 * @param ALT_ADDRESS ALT_ADDRESS bit
	 */
	public Adxl345(I2c module, boolean ALT_ADDRESS) throws ResultException {
		super(openI2c(module.getPointer(), ALT_ADDRESS));
	}

	/**
	 * Power up the sensor
	 */
	public void powerUp() throws ResultException {
		powerUp(getPointer());
	}

	public void close() throws ResultException {
		close(getPointer());
	}

	/**
	 * Read data
	 * @param values X, Y, Z to store (atleast of length 3)
	 */
	public void read(double[] values) throws ResultException,
					NullPointerException, IllegalArgumentException {
		if (values == null) {
			throw new NullPointerException("values is null");
		}

		if (!(values.length >= 3)) {
			throw new IllegalArgumentException("values should be atleast of length 3");
		}

		read(getPointer(), values);
	}

	/**
	 * Read data
	 * @return data
	 */
	public double[] read() throws ResultException {
		double[] values = new double[3];
		read(values);
		return values;
	}

	/**
	 * native glue method for b0_adxl345_i2c_open()
	 * @param module pointer to b0_i2c
	 * @param ALT_ADDRESS ALT_ADDRESS bit
	 * @return pointer to b0_adxl345
	 */
	private final static native ByteBuffer /* (b0_adxl345) */ openI2c(
		ByteBuffer /* (b0_i2c*) */ module, boolean ALT_ADDRESS)
			throws ResultException;

	/**
	 * native glue method for b0_adxl345_close()
	 * @param driver pointer to b0_adxl345
	 */
	private final static native void close(
		ByteBuffer /* (b0_adxl345*) */ driver) throws ResultException;

	/**
	 * native glue method for b0_adxl345_power_up()
	 * @param driver pointer to b0_adxl345
	 */
	private final static native void powerUp(
		ByteBuffer /* (b0_adxl345*) */ driver) throws ResultException;

	/**
	 * native glue method for b0_adxl345_read()
	 * @param driver pointer to b0_adxl345
	 */
	private final static native void read(
		ByteBuffer /* (b0_adxl345*) */ driver, double[] values)
			throws ResultException;
}
