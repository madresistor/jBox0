/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.driver;

import java.nio.ByteBuffer;

import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.I2c;
import com.madresistor.box0.driver.Driver;

public class L3gd20 extends Driver {
	public L3gd20(I2c module, boolean SAD0) throws ResultException {
		super(openI2c(module.getPointer(), SAD0));
	}

	public void powerUp() throws ResultException { powerUp(getPointer()); }
	public void close() throws ResultException { close(getPointer()); }

	public void read(double[] values) throws ResultException {
		/* TODO: enforce valus length array of minimum length 3 */
		read(getPointer(), values);
	}

	public double[] read() throws ResultException {
		double[] values = new double[3];
		read(values);
		return values;
	}

	private final static native ByteBuffer openI2c(ByteBuffer /* (b0_i2c*) */ module, boolean SAD0) throws ResultException;
	private final static native void close(ByteBuffer /* (b0_l3gd20*) */ driver) throws ResultException;
	private final static native void powerUp(ByteBuffer /* (b0_l3gd20*) */ driver) throws ResultException;
	private final static native void read(ByteBuffer /* (b0_l3gd20*) */ driver, double[] values) throws ResultException;
}
