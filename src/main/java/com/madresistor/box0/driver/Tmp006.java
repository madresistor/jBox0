/*
 * This file is part of jBox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * jBox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jBox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jBox0.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0.driver;

import java.nio.ByteBuffer;

import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.I2c;
import com.madresistor.box0.driver.Driver;

public class Tmp006 extends Driver {
	public static final double DUMMY_CALIB_FACTOR = 6.4e-14;

	public Tmp006(I2c module, double calib_factor, boolean ADR1, Adr0 ADR0)
			throws ResultException {
		super(open(module.getPointer(), calib_factor, ADR1, ADR0.getValue()));
	}

	public void close() throws ResultException { close(getPointer()); }

	public double read() throws ResultException {
		return read(getPointer());
	}

	public static enum Adr0 {
		GND(0),
		VCC(1),
		SDA(2),
		SCL(3);

		private final int m_value;
		private Adr0(int value) { m_value = value; }
		public int getValue() { return m_value; }

		static public Adr0 reverseLookup(int value) {
			switch(value) {
			case 0:
			return GND;
			case 1:
			return VCC;
			case 2:
			return SDA;
			case 3:
			return SCL;
			}

			throw new IllegalArgumentException("value out of range");
		}
	}

	private final static native ByteBuffer open(ByteBuffer /* (b0_i2c*) */ module, double calib_factor, boolean ADR1, int ADR0) throws ResultException;
	private final static native void close(ByteBuffer /* (b0_tmp006*) */ driver) throws ResultException;
	private final static native double read(ByteBuffer /* (b0_tmp006*) */ driver) throws ResultException;
}
